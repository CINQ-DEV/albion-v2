<?php

class edd_remote_auto_updater_acf_youtube
{
	var $settings;
	var $plugin_slug = "acf-youtube-field";
	var $ping_url = "https://halgatewood.com";
    var $plugin_path;

	function __construct( $plugin_path )
	{
		add_filter('pre_set_site_transient_update_plugins', array($this, 'check_for_update'));
		add_filter('plugins_api', array($this, 'plugin_api_call'), 10, 3);
		
		$this->plugin_path = $plugin_path;
    	
    	// This is for testing only!
    	//set_site_transient( 'update_plugins', null );
    
    	// Show which variables are being requested when query plugin API
    	//add_filter( 'plugins_api_result', array(&$this, 'debug_result'), 10, 3 );
	}

	function get_remote( $transient )
	{
        $info = false;

		// Get the remote info
		$ping_url = trailingslashit($this->ping_url) . "?edd_au_check_plugin=true";
		$ping_url .= "&email=" . urlencode( get_option('acf-youtube-updater-email-account') );
		$ping_url .= "&version=" . urlencode( $transient->checked[$this->plugin_path] );
		$ping_url .= "&slug=" . urlencode( $this->plugin_slug );
		
        $request = wp_remote_get( $ping_url );

        if( !is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200)
        {
            $info = @unserialize($request['body']);
            if($info)
            {
            	$info->slug = $this->settings['slug'];
            }
        }
        
        return $info;
	}
	
	function check_for_update( $transient )
	{
	    if( empty($transient->checked) ) return $transient;
        
        $info = $this->get_remote( $transient );
        if( !$info ) return $transient;

        $obj = new stdClass();
        $obj->slug 				= $info->slug;
        $obj->new_version 		= $info->version;
        $obj->url 				= $info->homepage;
        $obj->package 			= $info->download_link;
        $transient->response[ $this->plugin_path ] = $obj;

        return $transient;
	}
	
    function plugin_api_call( $false, $action, $arg )
    {
    	if( !isset($arg->slug) || $arg->slug != $this->settings['slug'] ) return $false;
    	if( $action == 'plugin_information' ) $false = $this->get_remote();     
        return $false;
    }
    
    function debug_result( $res, $action, $args ) {
    	echo '<pre>'.print_r($res,true).'</pre>';
    	return $res;
    }
}

