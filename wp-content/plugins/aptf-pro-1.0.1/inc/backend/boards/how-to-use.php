<div class="aptf-single-board-wrapper" id="aptf-how_to_use-board" style="display:none">
    <h3><?php _e('How to use', APTF_TD_PRO); ?></h3>
    <p>There are two methods to display your Twitter Feeds in your site using AccessPress Twitter Feed Pro Plugin.</p>
    <h4>Normal Feeds</h4>
    <p>For displaying Twitter Feeds in normal manner, you can use <br><br/>[ap-twitter-feed-pro]<br/><br/> shortcode or AccessPress Twitter Feed Pro Widget in any registered widget area from Appearance Widget Section.You can also pass template parameter in the shortcode such as <br/>
        [ap-twitter-feed-pro template="template-1/template-2/template-3/template-4/template-5... template-12" total_feeds="3" username="Place your twitter username here"]</p>
    
    <h4>Feeds In Slider</h4>
    <p>To display the twitter feeds, you can either use <br/><br />
        [ap-twitter-feed-pro-slider] <br/><br/> <strong>Shortcode</strong> or <strong>AccessPress Twitter Feed Pro Widget</strong> in any registered widget area from Appearance Widget Section.You can use template parameter in this shortcode too.</p>
    <p>Extra parameters that you can pass in slider shortcode are auto_slide="true/false" slide_duration="any duration in milliseconds:e.g: 1000" and controls="true/false" </p>
    <p>
        [ap-twitter-feed-pro-slider controls="true" slide_duration="2000" auto_slide="true" slider_mode="horizontal/fade" adaptive_height="true/false" total_feeds="any number greater than 1 and less than or equals to 20" username="Place your twitter username here"]
    </p>
    
    <h4>Feeds in ticker</h4>
    <p>To display the twitter feeds, you can either use <br/><br />
        [ap-twitter-feed-pro-ticker] <br/><br/> <strong>Shortcode</strong> or <strong>AccessPress Twitter Feed Pro Ticker Widget</strong> in any registered widget area from Appearance Widget Section.You can use template parameter in this shortcode too.</p>
    <p>Extra parameters that you can pass in ticker shortcode are controls="true" mouse_pause="true/false" ticker_speed="any duration in milliseconds:e.g: 1000" ticker_direction="up/down" visible_tweets="Any number greater than 1 and less than total tweets" total_feeds="any number greater than 1 and less than or equals to 20" </p>
    <p>
        [ap-twitter-feed-pro-ticker controls="true/false" mouse_pause="true/false" ticker_speed="any duration in milliseconds:e.g: 1000" ticker_direction="up/down" visible_tweets="Any number greater than 1 and less than total tweets" total_feeds="any number greater than 1 and less than or equals to 20"]
    </p>
    
    <p>For detailed documentation please visit <a href="https://accesspressthemes.com/documentation/documentationplugin-instruction-accesspress-twitter-feed/" target="_blank">here</a></p>
</div>