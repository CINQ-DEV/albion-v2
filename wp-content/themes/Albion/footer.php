<div class="container" style="bbackground:yellow">
  <div class="col-sm-12 col-md-12" >
    <?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
  </div>
</div>
<footer id="footer" class="clearfix">
  <div id="footer-widgets">
    <div class="container">
      <div id="footer-wrapper">
        <div class="row" >
          <div class="col-sm-6 col-md-4" id="opening">
            <?php include "library/parts/opening_hours.php"?>
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
            <?php endif; ?>
          </div>
          <!-- end widget1 -->
          
          <div class="col-sm-6 col-md-4" >
            <?php include "library/parts/next_event.php"?>
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
            <?php endif; ?>
          </div>
          <!-- end widget1 -->
          
          <div class="col-sm-6 col-md-4" >
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
            <?php endif; ?>
          </div>
          <!-- end widget1 --> 
          
        </div>
        <!-- end .row --> 
        
      </div>
      <!-- end #footer-wrapper --> 
      
    </div>
    <!-- end .container --> 
  </div>
  <!-- end #footer-widgets -->
  
  <div id="sub-floor">
    <div class="container">
      <div class="row">
        <div class="col-md-12 copyright"> &copy; <?php echo date('Y'); ?>
          <?php bloginfo( 'name' ); ?>
          |Site design - <a href="http://www.cinq.agency">CINQ agency</a> </div>
      </div>
      <!-- end .row --> 
    </div>
  </div>
</footer>
<!-- end footer --> 

<!-- all js scripts are loaded in library/bones.php -->
<?php wp_footer(); ?>
</body></html>