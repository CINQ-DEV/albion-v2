<h4>opening hours</h4>
<?php

// check if the repeater field has rows of data
if( have_rows('opening_hours','options') ):

 	// loop through the rows of data
    while ( have_rows('opening_hours','options') ) : the_row();

        // display a sub field value?>
<div> <span class="day" >
  <?php   the_sub_field('day_name','options');?>
  </span> <span class="time" >
  <?php the_sub_field('opening_time','options');?>
  </span> <span class="to">-</span> <span class="time" >
  <?php 	the_sub_field('closing_time','options');?>
  </span> </div>
<?php    endwhile;

else :

    // no rows found

endif;

?>
