<?php $n = 1;
$currentdate = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
?>

<h4>Next Event</h4>
<?php 
	 $args = array (


'meta_query'=> array(
	                    array(
	                      'key' => 'event_starts_sort_field',
	                      'compare' => '>=',
	                      'value' => $currentdate,
	                      'type' => 'DATE',
	                    )),
		    'post_type' => 'facebook_events',
	        'meta_key' => 'event_starts_sort_field',
           'orderby'=> 'meta_value',
            'order' => 'ASC',
			'posts_per_page'         => '1',
);
		$fbe_query = new WP_Query( $args );
		if( $fbe_query->have_posts() ): 
		while ( $fbe_query->have_posts() ) : $fbe_query->the_post();?>
<?php
$n++;
		$event_title = get_the_title();
		$event_starts = get_fbe_date('event_starts','l jS  F ');
		$event_starts_time = get_fbe_date('event_starts',' h:i A');
		$event_ends = get_fbe_date('event_ends','M j, Y @ g:i a');
		$event_ends_time = get_fbe_date('event_ends','M j, Y @ g:i a');
		$user_date = get_fbe_date('event_starts','d/m/Y');
		$today_date = date("d/m/Y");
		$permalink = get_permalink();
	
	?>
<?php
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
<div class="next_event_block "  style="background-image:url(<?php echo $feat_image?>)" >
  <div class="overlay_color ">
    <div class="inner-hover">
      <h4><?php echo $event_title; ?></h4>
      <p ><?php echo $event_starts ?> | <?php echo $event_starts_time ?></p>
      <a class="linkbox" href="<?php echo $permalink ?>">more info</a> <span class="left-bottom corner-border"></span> <span class="right-top corner-border"></span> <span class="left-top corner-border"></span> <span class="right-bottom corner-border"></span> </div>
  </div>
</div>
<div style="text-align:center;">
<a class="linkbox" href="/facebook-events">View all events</a>
</div>
<?php

	     endwhile;
		 endif;
			
	wp_reset_query();  

	?>
