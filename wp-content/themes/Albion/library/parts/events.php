
  <div class="row">
 <h2>Events</h2>
    <?php
// WP_Query arguments
$args = array (
	'post_type'              => array( 'event' ),
);

// The Query
$event = new WP_Query( $args );

// The Loop
if ( $event->have_posts() ) {
	while ( $event->have_posts() ) {
		$event->the_post(); ?>
    <div class="col-lg-4 box">
      <?php the_title() ?>
    </div>
    <?php }
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();

?>
  </div>

