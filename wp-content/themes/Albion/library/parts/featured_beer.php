<?php
$timecutoff = date("Y-m-d");
$args = array (
'posts_per_page' => 1,
	'post_type'              => 'beers',
	
	'meta_query' => array(
        array  (
            'key' => 'beer_featured',
            'value'=>'1'
        )
    )
	
	
	
	
);

$my_query = new WP_Query($args);

if ($my_query->have_posts()) : while ($my_query->have_posts()) :

$my_query->the_post();

?>

<?php $image = get_field('hero_image');
	
	
	?>
    




<div class="background" style="background-image :url( <?php echo $image['url']; ?>)">
<div class="overlay">
 <h6>This weeks guest</h6>
<h4>
  <?php the_title(); ?>
</h4>
<span class="date_format">

</span> <br/>
<span class="more" data-toggle="modal" data-target="#<?php the_slug();?>">MORE</span>
<?php include "library/parts/modal_events.php"; ?>
</div></div>
<?php endwhile; else: ?>
<?php _e('Sorry no featured events .'); ?>
<?php endif; ?>
