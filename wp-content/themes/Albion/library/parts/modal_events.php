<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="<?php the_slug();?>">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    
    <div class="row">
          <div class="col-md-4"><h4>
            <?php the_title(); ?>
          </h4></div>
          <div class="col-md-4"><?php $date = DateTime::createFromFormat('dmY', get_field('event_start_date'));?>
          <?php echo $date->format('l  ');?>
          <?php /*?><?php echo $date->format('jS \of F Y');?> <?php */?>
          <?php echo $date->format('jS');?> <br/>
          <?php the_field('event_start_time') ?> - <?php the_field('event_end_time') ?></div>
           <div class="col-md-4"><?php get_the_content(); ?></div>
        </div>
    
     
    
   
 
          
          
    </div>
  </div>
</div>