<?php
$timecutoff = date("Y-m-d");
$args = array (
'posts_per_page' => 1,
	'post_type'              => 'event',
	'order'                  => 'ASC',
	'orderby'                => 'event_start_date',
	'meta_query' => array(
        array  (
            'key' => 'event_featured',
            'value'=>'1'
        )
    )
	
	
	
	
);

$my_query = new WP_Query($args);

if ($my_query->have_posts()) : while ($my_query->have_posts()) :

$my_query->the_post();
$eventdate = get_post_meta($post->ID, "event_start_date", true);
$eventsortdate = get_post_meta($post->ID, "event_start_date", true);
?>
<?php if(!isset($currentMonth) || $currentMonth != date("m", strtotime($eventsortdate))){
    $currentMonth = date("m", strtotime($eventsortdate));
?>
<?php
}
?>
<?php $image = get_field('hero_image');
	
	
	?>

<div class="background" style="background-image :url( <?php echo $image['url']; ?>)" id="featured_home">
  <div class="overlay2">
    <h6>Featured Event</h6>
    <h4>
      <?php the_title(); ?>
    </h4>
    <span class="date_format">
    <?php $date = DateTime::createFromFormat('dmY', get_field('event_start_date'));?>
    <?php echo $date->format('l  ');?> <?php echo $date->format('jS \of F Y');?> at
    <?php the_field('event_start_time') ?>
    </span> <br/>
    
     <?php if ( get_post_meta($post->ID, 'ticketed_event', true) ) {?>
       <a href="<?php echo esc_url( get_permalink() ); ?>" > <span class="icon-ticket4">BUY TICKETS</span> </a>
        <?php } else { ?>

         
    
    
    <a href="<?php echo esc_url( get_permalink() ); ?>" class="more" > MORE</a>
    <?php }?>
   
  </div>
</div>
<?php endwhile; else: ?>
<?php _e('Sorry no featured events .'); ?>
<?php endif; ?>
