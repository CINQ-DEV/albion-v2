
<div  class="container" >
  <?php /*?>  <div id="pig"></div><?php */?>
  <div class="row">
    <div class="col-md-6 col-lg-6 event_month"  >
      <?php include "featured_event_home.php"; ?>
      <?php include "next_event_home.php"; ?>
    </div>
    <div class="col-md-6 col-lg-6 event_month"  >
      <?php
$timecutoff = date("Y-m-d");
$args = array (
	'post_type'              => 'event',
	'order'                  => 'ASC',
	'orderby'                => 'event_start_date',
	'meta_query' => array(
        array(
            'key' => 'event_start_date',
            'value' => '$timecutoff',
            'compare' => '>='
        )
    )
	
);

$my_query = new WP_Query($args);

if ($my_query->have_posts()) : while ($my_query->have_posts()) :

$my_query->the_post();
$eventdate = get_post_meta($post->ID, "event_start_date", true);
$eventsortdate = get_post_meta($post->ID, "event_start_date", true);
?>
      <?php if(!isset($currentMonth) || $currentMonth != date("m", strtotime($eventsortdate))){
    $currentMonth = date("m", strtotime($eventsortdate));
?>
      <div class="col-md-12 col-lg-12 event_month"  >
        <hr/>
      </div>
      <?php
}
?>
      <div >
        <div class="col-md-12 event_single" class="more" >
        <a href="<?php echo esc_url( get_permalink() ); ?>" >
        <div class="col-md-3  date">
          <?php $date = DateTime::createFromFormat('dmY', get_field('event_start_date'));?>
          <?php echo $date->format('D');?><?php echo $date->format(' M j');?> </div>
        <div class="col-md-3 date">
          <?php the_field('event_start_time') ?>
        </div>
        <div class="col-md-5">
          <h4 >
            <?php the_title(); ?>
          </h4>
        </div>
        <div class="col-md-1">
          <?php if ( get_post_meta($post->ID, 'event_featured', true) ) : ?>
          <?php endif; ?>
          <?php if ( get_post_meta($post->ID, 'ticketed_event', true) ) : ?>
          <span class="icon-ticket4"></span>
          <?php endif; ?>
        </div>
        </a> </div>
    </div>
    <?php endwhile; else: ?>
    <?php _e('No Events Scheduled! .'); ?>
    <?php endif; ?>
  </div>
</div>
</div>
