<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}


	
	
	
	
	// registration code for artists post type
	function register_artists_posttype() {
		$labels = array(
			'name' 				=> _x( 'Artists', 'post type general name' ),
			'singular_name'		=> _x( 'Artist', 'post type singular name' ),
			'add_new' 			=> __( 'Add New' ),
			'add_new_item' 		=> __( 'Artist' ),
			'edit_item' 		=> __( 'Artist' ),
			'new_item' 			=> __( 'Artist' ),
			'view_item' 		=> __( 'Artist' ),
			'search_items' 		=> __( 'Artist' ),
			'not_found' 		=> __( 'Artist' ),
			'not_found_in_trash'=> __( 'Artist' ),
			'parent_item_colon' => __( 'Artist' ),
			'menu_name'			=> __( 'Artists' )
		);
		
		$taxonomies = array();

		$supports = array('title','editor','author','thumbnail','excerpt','custom-fields','comments','revisions');
		
		$post_type_args = array(
			'labels' 			=> $labels,
			'singular_label' 	=> __('Artist'),
			'public' 			=> true,
			'show_ui' 			=> true,
			'publicly_queryable'=> true,
			'query_var'			=> true,
			'exclude_from_search'=> false,
			'show_in_nav_menus'	=> true,
			'capability_type' 	=> 'post',
			'has_archive' 		=> true,
			'hierarchical' 		=> false,
			'rewrite' 			=> array('slug' => 'artists', 'with_front' => false ),
			'supports' 			=> $supports,
			'menu_position' 	=> 5,
			'menu_icon' 		=> get_stylesheet_directory_uri().'/library/images/microphone.png',
			'taxonomies'		=> $taxonomies
		 );
		 register_post_type('artists',$post_type_args);
	}
	add_action('init', 'register_artists_posttype');
	
	// registration code for eventtype taxonomy
		function register_eventtype_tax() {
			$labels = array(
				'name' 					=> _x( 'Event types', 'taxonomy general name' ),
				'singular_name' 		=> _x( 'Event type', 'taxonomy singular name' ),
				'add_new' 				=> _x( 'Add New event type', 'event type'),
				'add_new_item' 			=> __( 'Add New event type' ),
				'edit_item' 			=> __( 'Edit event type' ),
				'new_item' 				=> __( 'New event type' ),
				'view_item' 			=> __( 'View event type' ),
				'search_items' 			=> __( 'Search event types' ),
				'not_found' 			=> __( 'No event type found' ),
				'not_found_in_trash' 	=> __( 'No event type found in Trash' ),
			);
			
			$pages = array('event');
			
			$args = array(
				'labels' 			=> $labels,
				'singular_label' 	=> __('event type'),
				'public' 			=> true,
				'show_ui' 			=> true,
				'hierarchical' 		=> true,
				'show_tagcloud' 	=> true,
				'show_in_nav_menus' => true,
				'rewrite' 			=> array('slug' => 'event-types', 'with_front' => false ),
			 );
			register_taxonomy('eventtype', $pages, $args);
		}
		add_action('init', 'register_eventtype_tax');
	
	function register_beers_posttype() {
		$labels = array(
			'name' 				=> _x( 'Beers', 'post type general name' ),
			'singular_name'		=> _x( 'Beer', 'post type singular name' ),
			'add_new' 			=> __( 'Add New' ),
			'add_new_item' 		=> __( 'Beer' ),
			'edit_item' 		=> __( 'Beer' ),
			'new_item' 			=> __( 'Beer' ),
			'view_item' 		=> __( 'Beer' ),
			'search_items' 		=> __( 'Beer' ),
			'not_found' 		=> __( 'Beer' ),
			'not_found_in_trash'=> __( 'Beer' ),
			'parent_item_colon' => __( 'Beer' ),
			'menu_name'			=> __( 'Beers' )
		);
		
		$taxonomies = array();

		$supports = array('title','editor','author','thumbnail','excerpt','custom-fields','comments','revisions');
		
		$post_type_args = array(
			'labels' 			=> $labels,
			'singular_label' 	=> __('Beer'),
			'public' 			=> true,
			'show_ui' 			=> true,
			'publicly_queryable'=> true,
			'query_var'			=> true,
			'exclude_from_search'=> false,
			'show_in_nav_menus'	=> true,
			'capability_type' 	=> 'post',
			'has_archive' 		=> true,
			'hierarchical' 		=> false,
			'rewrite' 			=> array('slug' => 'beer', 'with_front' => true ),
			'supports' 			=> $supports,
			'menu_position' 	=> 5,
			'menu_icon' 		=> get_stylesheet_directory_uri().'/library/images/mug.png',
			'taxonomies'		=> $taxonomies
		 );
		 register_post_type('beers',$post_type_args);
	}
	add_action('init', 'register_beers_posttype');
	
	
	
	// Register Custom Taxonomy
function music_genre_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Genres', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Genre', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Genre', 'text_domain' ),
		'all_items'                  => __( 'All Genres', 'text_domain' ),
		'parent_item'                => __( 'Parent Genre', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Genre:', 'text_domain' ),
		'new_item_name'              => __( 'New Genre Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Genre', 'text_domain' ),
		'edit_item'                  => __( 'Edit Genre', 'text_domain' ),
		'update_item'                => __( 'Update Genre', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove genres', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search genres', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'genre', array( 'artists' ), $args );

}
add_action( 'init', 'music_genre_taxonomy', 0 );

?>