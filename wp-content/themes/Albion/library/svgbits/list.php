 <svg id="trigger-overlay" class="list" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.5 10.5">
<path class="list-content" d="M5.25,5.25V7.88h10.5V5.25H5.25Zm0,6.56h10.5V9.19H5.25v2.63Zm0,3.94h10.5V13.13H5.25v2.63Z" transform="translate(-5.25 -5.25)"/>
</svg>