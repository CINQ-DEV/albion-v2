<?php
/*
Template Name: Page - Right Sidebar
*/
?>
<?php get_header(); ?>

<div class="main">
<div class="container main_content">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h1>
      <?php the_title()?>
    </h1>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  ></div>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
  
  
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
  <div class="content_block_page"  style="background-image:url(<?php echo $feat_image?>);height:400px" ></div>
    <?php the_content(); ?>
    <?php endwhile; ?>
    <?php else : ?>
    <header>
      <h1>
        <?php _e("Not Found", "bonestheme"); ?>
      </h1>
    </header>
    <section class="post_content">
      <p>
        <?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?>
      </p>
    </section>
    <?php endif; ?>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <?php get_sidebar(); ?>
  </div>
  <!-- end #main --> 
  
  <!-- end #content --> 
  
</div>
<!-- end .container -->

<?php get_footer(); ?>
