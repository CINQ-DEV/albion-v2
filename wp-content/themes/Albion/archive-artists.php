<?php get_header(); ?>

<div id="chalk_events" class="main_content">




<div class="featured_tag">
  <div id="pig"></div>
  <div  class="container">
    <div class="row">

    
    
    
    
      <?php
$timecutoff = date("Y-m-d");
$args = array (
	'post_type'              => 'artist',
	'order'                  => 'ASC',
	'orderby'                => 'event_start_date',
	'meta_query' => array(
        array(
            'key' => 'event_start_date',
            'value' => '$today',
            'compare' => '>='
        )
    )
	
);

$my_query = new WP_Query($args);

if ($my_query->have_posts()) : while ($my_query->have_posts()) :

$my_query->the_post();
$eventdate = get_post_meta($post->ID, "event_start_date", true);
$eventsortdate = get_post_meta($post->ID, "event_start_date", true);
?>
      <?php if(!isset($currentMonth) || $currentMonth != date("m", strtotime($eventsortdate))){
    $currentMonth = date("m", strtotime($eventsortdate));
?>
      <div class="col-md-12 col-lg-12 event_month"  >
        <h3><?php echo date("F", strtotime($eventsortdate)); ?></h3>
      </div>
      <?php
}
?>
      <div class="col-md-6 col-lg-6 event_box   <?php if ( get_post_meta($post->ID, 'event_featured', true) ) : ?>
	featured
<?php endif; ?>" >
        <div class="box_image" >
        
         <?php if ( get_post_meta($post->ID, 'event_featured', true) ) : ?>
	<div class="featured_tag">Featured Event</div>
<?php endif; ?>
        
          <h4>
            <?php the_title(); ?>
          </h4>
        </div>
        <div class="box_text">
          <?php $date = DateTime::createFromFormat('dmY', get_field('event_start_date'));?>
          <?php echo $date->format('l  ');?>
          <?php /*?><?php echo $date->format('jS \of F Y');?> <?php */?>
          <?php echo $date->format('jS');?> <br/>
          <?php the_field('event_start_time') ?>
          -
          <?php the_field('event_end_time') ?>
          
         
          
          
        
          
          <br/>
          <span class="more" data-toggle="modal" data-target="#<?php the_slug();?>">MORE</span>
          <?php include "library/parts/modal_events.php"; ?>
        </div>
      </div>
      <?php endwhile; else: ?>
      <?php _e('No Events Scheduled! .'); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
