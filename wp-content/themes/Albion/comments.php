<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>

<div class="comment-section">
	<h2 class="comment-title"><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></h2>

	<ul class="comment-tree">
		<?php wp_list_comments(''); ?>
	</ul>
	<?php
	// Are there comments to navigate through?
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<footer class="navigation comment-navigation" role="navigation">
			<!--<h1 class="screen-reader-text section-heading"><?php esc_html_e( 'Comment navigation', 'montblanc' ); ?></h1>-->
			<div class="previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'montblanc' ) ); ?></div>
			<div class="next right"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'montblanc' ) ); ?></div>
		</footer><!-- .comment-navigation -->
	<?php endif; // Check for comment navigation ?>

	<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'montblanc' ); ?></p>
	<?php endif; ?>
	
	<?php
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$comment_args = array(
		'title_reply'=> 'Write a Comment',
		'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '<div class="row">
				<div class="col-md-4">
					<input type="text" name="author"   placeholder="Name*" id="name" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' />
				</div>
			',
			'email' => '
				<div class="col-md-4">
					<input id="mail" name="email"   placeholder="Email*" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' />
				</div>
			',
			'url' => '<div class="col-md-4">
				<input id="website" name="url"  placeholder="Website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"  /></div>
			</div>
			'
		)),
		'comment_field' => '<textarea   rows="7" id="comment" placeholder="Your Message*"  name="comment"'.$aria_req.'></textarea>',
		'label_submit' => 'Post Comment',
		'comment_notes_after' => '',
	);
	?>
	
	<?php global $post; ?>
	<?php if('open' == $post->comment_status){ ?>

		<?php comment_form($comment_args); ?>

	<?php } ?>
	</div>	