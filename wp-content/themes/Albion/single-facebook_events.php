<?php get_header(); ?>
<?php $currentpost = get_the_ID(); ?>
<?php $socialtag = get_field('social_media','option')?>
<?php echo $socialtag ?>

<div class="main">
  <div class="container main_content single-facebook">
    <div class="row" >
      <?php 
	 
		$args = array (
		    'post_type' => 'facebook_events',
			'posts_per_page' => -1,
			'order' => 'ASC',
			'p'                      => $currentpost,
		);
		
		$fbe_query = new WP_Query( $args );
		if( $fbe_query->have_posts() ): 
		while ( $fbe_query->have_posts() ) : $fbe_query->the_post();

		$event_title = get_the_title();
		
		$event_image = get_fbe_image('list');
		$event_title = get_the_title();
		$event_desc =  get_the_content();
		$event_image = get_fbe_image();
		$event_excerpt = get_the_excerpt() ;
		$event_starts = get_fbe_date('event_starts','l jS \of F Y');
		$event_starts_time = get_fbe_date('event_starts',' h:i A');
		$event_ends = get_fbe_date('event_ends','M j, Y @ g:i a');
		$event_ends_time = get_fbe_date('event_ends','M j, Y @ g:i a');
		$user_date = get_fbe_date('event_starts','d/m/Y');
		$today_date = date("d/m/Y");
		$permalink = get_permalink();
	
	?>
      <div class="col-xs-12 col-sm-12 col-md-12  col-lg-8 col-lg-offset-2 block-single  vig" style=" background-image:url(<?php echo $event_image ?>);"> </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
        <p class="date">
          <?php  echo $event_starts?>
          at
          <?php  echo $event_starts_time ?>
          <a href="/facebook-events/" class="all-events linkbox"> All Events</a> </p>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
        <h1><?php echo $event_title; ?></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 ">
      <div class="content_block">
        <?php the_content(); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 ">
         <?php comments_template(); ?>
      </div>
    </div>
    
   
    <?php

	     endwhile;
		 endif;
			
	wp_reset_query();  ?>
  </div>
</div>
<?php get_footer(); ?>
