<?php
/*
Template Name: Home Page Template
*/
?>
<?php get_header(); ?>

<div class="main cl-effect-2">
  <div class="container main_content">
    <div class="row" >
      <?php

// check if the flexible content field has rows of data
if( have_rows('homepage_items') ):

     // loop through the rows of data
    while ( have_rows('homepage_items') ) : the_row();
	$permalink = get_permalink();

        if( get_row_layout() == 'picture_block' ):?>
      <?php $mylink = get_sub_field('layout_link');?>
      <a href="<?php echo $mylink[url] ?>">
      <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block  vig hidden-sm hidden-xs" style=" background-image:url(<?php the_sub_field('layout_image'); ?>);"> </div>
      </a>
      <?php   elseif( get_row_layout() == 'text_block' ): ?>
      <?php $mylink = get_sub_field('layout_link');?>
      <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block" >
      
        <div class="hover-box">
          <div class="inner-hover">
            <h2><a href="<?php echo $mylink[url] ?>">
              <?php the_sub_field('main_text'); ?>
              </a></h2>
            <p>
              <?php the_sub_field('sub_text'); ?>
            </p>
            <a href="<?php $mylink; ?>" class="linkbox">more</a> <span class="left-bottom corner-border"></span> <span class="right-top corner-border"></span> <span class="left-top corner-border"></span> <span class="right-bottom corner-border"></span> </div>
        </div>
      </div>
      <?php	

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
