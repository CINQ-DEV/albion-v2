<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->

<head>
<meta charset="utf-8">
<?php // Google Chrome Frame for IE ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>
<?php if (is_front_page()) { bloginfo('name'); } else { wp_title(''); } ?>
</title>
<?php // mobile meta (hooray!) ?>
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png?v=2">
<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
<?php // or, set /favicon.ico for IE10 win ?>
<meta name="msapplication-TileColor" content="#f01d4f">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
<?php // end of wordpress head ?>
<?php // drop Google Analytics Here ?>
<?php get_field('pininterest','option');?>
<?php the_field('google_analytics','option');?>
<?php // end analytics ?>
</head>
<body <?php body_class(); ?>>
<header class="header cl-effect-1"  id="header">
  <div class="container">
    <div class="row">
    <div class="col-xs-0 col-sm-0 col-md-2  col-lg-2 "> </div>
    <div class="col-xs-9 col-sm-10 col-md-8 col-lg-8 "> <a href="/" >
      <?php include "library/svgbits/logo_dark.php"?>
      </a> </div>
	  <div class="col-xs-3 col-sm-2 col-md-2  col-lg-2 "> <?php include "library/svgbits/list.php"?> </div></div>
  </div>
</header>
<!--Main Menu-->

<div class="overlay overlay-slidedown">
  <button type="button" class="overlay-close">Close</button>
  <nav>
    <?php wp_nav_menu( array( 'theme_location' => 'main-nav') ); ?>
  </nav>
</div>

<!--Main Menu--> 
