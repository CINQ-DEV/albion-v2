<?php get_header(); ?>

<div class="main">
<div class="container main_content single-facebook">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="row" >
  <div class="col-xs-12 col-sm-12 col-md-12  col-lg-8 col-lg-offset-2 block-single  vig" style=" background-image:url(<?php echo $event_image ?>);"> </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
    <p class="date">
      <time class="updated" datetime="<?php get_the_time('Y-m-j') ?>"><?php echo get_the_time(get_option('date_format')) ?></time>
      at
      <?php  echo $event_starts_time ?>
      <a href="/news/" class="all-events linkbox"> All News</a> </p>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
    <h1>
      <?php the_title()?>
    </h1>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 ">
    <div class="content_block">
      <?php the_content(); ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 ">
    <?php comments_template(); ?>
  </div>
</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php get_footer(); ?>
