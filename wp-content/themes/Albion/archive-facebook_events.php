<?php /*?>$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$postcnt = 0; query_posts('cat=3,4,5,6&order=desc&orderby=meta_value&meta_key=date');
	$lastPost=$wp_query->post_count;<?php */?>
<?php get_header(); ?>
<?php $n = 1;
$currentdate = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
?>

<div class="main">
  <div class="container main_content">
    
    <div class="col-xs-12col-sm-12 col-md-12 col-lg-12">
      <h1>Upcoming Events at the Albion</h1>
    </div>
  
    <?php 
	 $args = array (


'meta_query'=> array(
	                    array(
	                      'key' => 'event_starts_sort_field',
	                      'compare' => '>=',
	                      'value' => $currentdate,
	                      'type' => 'DATE',
	                    )),
		    'post_type' => 'facebook_events',
			'posts_per_page' => $max,
			'paged' => $paged,
	        'meta_key' => 'event_starts_sort_field',
           'orderby'=> 'meta_value',
            'order' => 'ASC',



);
		$fbe_query = new WP_Query( $args );
		if( $fbe_query->have_posts() ): 
		while ( $fbe_query->have_posts() ) : $fbe_query->the_post();?>
    <?php
$n++;
		$event_title = get_the_title();
		$event_desc =  get_the_content();
		$event_image = get_fbe_image();
		$event_excerpt = get_the_excerpt() ;
		$datef = get_fbe_date('event_starts','l jS \of F Y');
		$event_starts = get_fbe_date('event_starts','l jS \of F Y');
		$event_starts_time = get_fbe_date('event_starts',' h:i A');
		$event_ends = get_fbe_date('event_ends','M j, Y @ g:i a');
		$event_ends_time = get_fbe_date('event_ends','M j, Y @ g:i a');
		$user_date = get_fbe_date('event_starts','d/m/Y');
		$today_date = date("d/m/Y");
		$permalink = get_permalink();
	
	?>
    <div class="row" >
      <?php  if ($n % 2 == 0) {?>
      <a href="<?php echo $permalink ?>" class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block  vig" style=" background-image:url(<?php echo $event_image ?>);">
      <!--<div class="triright"></div>-->
      </a>
      <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block" >
        <div class="hover-box">
          <div class="inner-hover">
            <h2><a href="<?php echo $permalink ?>"> <?php echo $event_title; ?></a> </h2>
            <p class="date"><?php echo $event_starts ?></p>
            <p class="date"><?php echo $event_starts_time ?></p>
            <p ><?php echo $event_excerpt?></p>
            <h4><a href="<?php echo $permalink ?>" class="linkbox">more info</a></h4>
            <span class="left-bottom corner-border"></span> <span class="right-top corner-border"></span> <span class="left-top corner-border"></span> <span class="right-bottom corner-border"></span> </div>
        </div>
      </div>
      <?php  }?>
      <?php  if ($n % 2 == 1) {?>
      <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block" >
        <div class="hover-box">
          <div class="inner-hover">
            <h2><a href="<?php echo $permalink ?>"><?php echo $event_title; ?></a></h2>
            <p class="date"><?php echo $event_starts ?></p>
            <p class="date"><?php echo $event_starts_time ?></p>
            <p><?php echo $event_excerpt?></p>
            <h4><a href="<?php echo $permalink ?>" class="linkbox">more info</a></h4>
            <span class="left-bottom corner-border"></span> <span class="right-top corner-border"></span> <span class="left-top corner-border"></span> <span class="right-bottom corner-border"></span> </div>
        </div>
      </div>
      <a href="<?php echo $permalink ?>" class="col-xs-12 col-sm-6 col-md-6  col-lg-6 block  vig" style=" background-image:url(<?php echo $event_image ?>);">
    <!--  <div class="trileft"></div>-->
      </a>
      <?php  }?>
    </div>
    <?php

	     endwhile;
		 endif;
			
	wp_reset_query();  

	?>
  </div>
</div>
<?php get_footer(); ?>
