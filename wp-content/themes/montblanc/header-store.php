<?php 
	global $montblanc_options;
?>
<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container relative">
					<div class="navbar-header">
						
						<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
						<?php if($montblanc_options['logo']['url']!=''){ ?>
							<img src="<?php echo esc_attr($montblanc_options['logo']['url']); ?>" alt="<?php bloginfo('name'); ?>">
						<?php }else{ ?>
							<?php bloginfo('name'); ?>
						<?php } ?>
						</a>
						
					</div>
					
						<a href="#" class="open-menu">
							<span></span>
							<span></span>
							<span></span>
							<span class="close-menu"></span>
						</a>

						
							<?php if(is_active_sidebar('top-cart')){ ?>
							<?php if (class_exists('Woocommerce')) { ?>
							
							<div class="mini-cart hidden-xs">
							<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart','montblanc' ); ?>">$ <?php echo WC()->cart->get_cart_total(); ?> <span class="count"><?php echo sprintf (_n( '%d ', '%d ', WC()->cart->cart_contents_count ,'montblanc'), WC()->cart->cart_contents_count ); ?></span> <img src="<?php echo get_template_directory_uri(); ?>/images/cart.png" /></a>
								<?php
											
									dynamic_sidebar('top-cart');
								

								?>
								
						
							<?php } } ?>

							</div>
						
					
				</div>
			</div>
		</header>
		<!-- End Header -->

		<section class="right-nav"> 
          
		  <!--======= RIGHT SIDE BAR  =========-->
		  
		  <div class="navi_right" id="nav_right"> <a href="#." class="nav_close"></a>
		    <div class="side-bar-right">


		      <div class="site-nav">
				
					<?php if($montblanc_options['logo-alt']['url']!=''){ ?>
					<img src="<?php echo esc_attr($montblanc_options['logo-alt']['url']); ?>" alt="<?php bloginfo('name'); ?>">
					<?php } ?>
					<?php
				
						$defaults1= array(
							'theme_location'  => 'primary',
							'menu'            => '',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'nav navbar-nav',
							'menu_id'         => '',
							'echo'            => true,
							 'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							 //'walker'            => new wp_bootstrap_navwalker(),
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
						);
						if ( has_nav_menu( 'primary' ) ) {
							wp_nav_menu( $defaults1 );
					} ?>
					

					<ul class="social_icons">
					   <?php if($montblanc_options['facebook']!=''){ ?>
					  <li class="facebook"><a  href="<?php echo esc_url($montblanc_options['facebook']); ?>"><i class="fa fa-facebook"></i></a></li>
					  <?php } ?>
					  <?php if($montblanc_options['twitter']!=''){ ?>
					  <li class="twitter"><a href="<?php echo esc_url($montblanc_options['twitter']); ?>"><i class="fa fa-twitter"></i></a></li>
					  <?php } ?>
					 <?php if($montblanc_options['istagram']!=''){ ?>
						<li class="instagram"><a href="<?php echo esc_url($montblanc_options['twitter']); ?>"><i class="fa fa-instagram"> </i></a></li>
						<?php } ?>
					  <?php if($montblanc_options['google']!=''){ ?>
					  <li class="google"><a  href="<?php echo esc_url($montblanc_options['google']); ?>"><i class="fa fa-google-plus"></i></a></li>
					  <?php } ?>
					  <?php if($montblanc_options['linkedin']!=''){ ?>
					  <li class="linkedin"><a  href="<?php echo esc_url($montblanc_options['linkedin']); ?>"><i class="fa fa-linkedin"></i></a></li>
					  <?php } ?>
					  <?php if($montblanc_options['pinterest']!=''){ ?>
					  <li class="pinterest" ><a href="<?php echo esc_url($montblanc_options['pinterest']); ?>"><i class="fa fa-pinterest"></i></a></li>
					  <?php } ?>
					</ul>
			
			</div>

		      <div class="side-bar"> 
		        <?php get_sidebar('float'); ?>
		      </div>

		    </div>
		  </div>
		</section>