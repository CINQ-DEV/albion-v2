<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php 
global $montblanc_options, $woocommerce, $wp_query; 
?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
	<?php if($montblanc_options['favicon']['url']!=''){ ?>
	<link rel="icon" href="<?php echo esc_url($montblanc_options['favicon']['url']); ?>" type="image/x-icon">
	<?php } ?>
	<?php } ?>
	
	<?php if($montblanc_options['apple_icon']['url']!=''){ ?>
	<link rel="apple-touch-icon" href="<?php echo esc_url($montblanc_options['apple_icon']['url']); ?>" />
	<?php } ?>
	<?php if($montblanc_options['apple_icon_57']['url']!=''){ ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url($montblanc_options['apple_icon_57']['url']); ?>">
	<?php } ?>
	<?php if($montblanc_options['apple_icon_72']['url']!=''){ ?>
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url($montblanc_options['apple_icon_72']['url']); ?>">
	<?php } ?>
	<?php if($montblanc_options['apple_icon_114']['url']!=''){ ?>
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url($montblanc_options['apple_icon_114']['url']); ?>">
	<?php } ?>

	<?php wp_head(); ?>

</head>
<?php 

global $montblanc_options, $post;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}

?>

<body <?php body_class($version); ?>>

	<!-- Container -->
	<div id="container">
		<?php 
		  

		  if($version!=null and $version!='default'){
		    
		    get_template_part('header', $version);


		  }else{ 
		?>
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
						<?php if($montblanc_options['logo']['url']!=''){ ?>
							<img src="<?php echo esc_attr($montblanc_options['logo']['url']); ?>" alt="<?php bloginfo('name'); ?>">
						<?php }else{ ?>
							<?php bloginfo('name'); ?>
						<?php } ?>
						</a>
						
					</div>
					<div class="navbar-collapse collapse">
						<a href="#" class="open-menu">
							<span></span>
							<span></span>
							<span></span>
							<span class="close-menu"></span>
						</a>

						<?php
						
							$defaults1= array(
								'theme_location'  => 'primary',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'nav navbar-nav navbar-right',
								'menu_id'         => '',
								'echo'            => true,
								 'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
								 'walker'            => new wp_bootstrap_navwalker(),
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
							);
							if ( has_nav_menu( 'primary' ) ) {
								wp_nav_menu( $defaults1 );
						} ?>

					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
		<?php } ?>