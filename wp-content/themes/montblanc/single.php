<?php get_header();  ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}

?>
		<!-- content 
			================================================== -->
		<div id="content">
		<?php while(have_posts()) : the_post(); ?>
		<?php if($version=='vertical'){ ?>
		

			<div class="blog-gal">
			<?php if(has_post_thumbnail()){ ?>
				<?php the_post_thumbnail(); ?>
			<?php }else{ ?>
				
			<?php } ?>
			</div>
		


			<!-- blog-section
			================================================== -->
			<div class="single-blog-section">


				<div class="blog-box">
					<div class="blog-title">
						<p> <?php the_time(get_option( 'date_format' )); ?> <?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?> <?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
						<h1><?php the_title(); ?></h1>
					</div>
					<div class="blog-post single-post">
						<?php the_content(); ?>
						<?php
                        $defaults = array(
                          'before'           => '<div id="page-links"><strong>Page: </strong>',
                          'after'            => '</div>',
                          'link_before'      => '<span>',
                          'link_after'       => '</span>',
                          'next_or_number'   => 'number',
                          'separator'        => ' ',
                          'nextpagelink'     => esc_html__( 'Next page','montblanc' ),
                          'previouspagelink' => esc_html__( 'Previous page','montblanc' ),
                          'pagelink'         => '%',
                          'echo'             => 1
                        );
                       ?>
                      <?php wp_link_pages($defaults); ?>
                      <?php if(has_tag()){ ?>
                        <?php the_tags('<ul class="single-post-tags"><li><span>Tags: </span> </li><li>',' </li>, <li>','</li></ul>'); ?>
                      <?php } ?>  
						
						<div class="share-box">
						  
						  <ul class="share-this-post">
						    <li class="facebook"><a onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook-square"></i></a></li>
				              <li class="twitter"><a  onclick="window.open('http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>"><i class="fa fa-twitter"></i></a></li>
				              <li  class="pinterest"><a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a></li>
				              <li  class="pinterest"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest"></i></a></li>
				              <li class="linkedin"><a onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"><i class="fa fa-linkedin"></i></a></li>
						  </ul>
						  </ul>
						</div>

					</div>
					<div class="pre-next-post">
					  <div class="row">
					  	 <?php
			              $prev_post = get_adjacent_post(false, '', true);
			              $next_post = get_adjacent_post(false, '', false); 
			             ?>
			             
					    <!--=======  PREVIS POST =========-->
					    <div class="col-sm-6">
					    <?php if($prev_post!=null){ ?>
					    	<div class="row">
					    	 <div class="col-xs-3">
					    	 	<?php if(montblanc_thumbnail_url($size='', $id=$prev_post->ID)!=''){ ?>
					    	 		<img src="<?php echo bfi_thumb( montblanc_thumbnail_url($size='', $id=$prev_post->ID), array( 'width' => 70, 'height' => 70 ) ); ?>" class="pull-left" />
					    	 	<?php }else{ ?>
					    	 		<img  alt="<?php the_title(); ?>" src="http://placehold.it/70x70"  class="img-responsive" > 
					    	 	<?php } ?>
							      
							  </div>
						      <div class="prev-post col-xs-9">
						      <i class="fa fa-long-arrow-left"></i>  <span><?php esc_html_e('Previous Post','montblanc'); ?></span> 
						      <a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>"><?php echo esc_html($prev_post->post_title); ?> </a>
						      </div>
						  </div>
						<?php } ?>
					    </div>

					    <!--=======  NEXT POST =========-->
					    <div class="col-sm-6">
					    	<?php if($next_post!=null){ ?>
					    	<div class="row">
					     	<div class="next-post col-xs-9">
						        <span><?php esc_html_e('Next Post','montblanc'); ?></span> <i class="fa fa-long-arrow-right"></i> 
					        	<a href="<?php echo esc_url(get_permalink($next_post->ID)); ?>" title="<?php echo esc_attr($next_post->post_title); ?>"><?php echo esc_html($next_post->post_title); ?></a> 
					         </div>
					        <div class="col-xs-3">
					        <?php if(montblanc_thumbnail_url($size='', $id=$next_post->ID)!=''){ ?>
				    	 		<img src="<?php echo bfi_thumb( montblanc_thumbnail_url($size='', $id=$next_post->ID), array( 'width' => 70, 'height' => 70 ) ); ?>" class="pull-left" />
				    	 	<?php }else{ ?>
				    	 		<img  alt="<?php the_title(); ?>" src="http://placehold.it/70x70"  class="img-responsive" > 
				    	 	<?php } ?>
					        </div>	
					     </div>
					     <?php } ?>
					    </div>
					  </div>
					</div>

					<?php comments_template(); ?>
				</div>


			</div>
			<!-- End blog-section -->

		<?php }else{ ?>
		<?php if($version=='boxed'){ ?>
			<!-- page-banner-section
				================================================== -->
			<div class="section-content gallery-post">
			
				<div class="container">
			
					<?php if(get_post_format()=='gallery'){ ?>
					<?php 
					$gallery = get_post_meta(get_the_ID(), '_cmb_post_slider', true);
					?>
					<?php if(count($gallery)>0 and $gallery!=''){ ?>

					<div class="post-slider">
						<?php foreach($gallery as $img) {?>
						<div class="slide"><img src="<?php echo esc_attr($img); ?>" alt="<?php the_title(); ?>" /></div>
						<?php } ?>
						
						
						
					</div>
					<?php } ?>
					<?php }else{ 
						the_post_thumbnail();
					} ?>
				
				</div>
				
			</div>
			<!-- End page-banner-section -->
			<?php }else{ ?>
				<!-- page-banner-section
				================================================== -->
			<div class="section-content page-banner-section blog-banner single-post" <?php global $post; if(get_post_meta($post->ID, '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta($post->ID, '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">
					<div class="page-banner-box">
						<p class="post-meta">  <?php the_time(get_option( 'date_format' )); ?> <?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?> <?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
						<h1><?php the_title(); ?></h1>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->
			<?php } ?>
			<!-- blog-section
				================================================== -->
			<div class="section-content single-blog-section">
				<div class="container">
					<div class="row">
						<?php 

							if($version=='boxed' or $version=='vertical'){ 
								$class="col-md-12";
							}else{
								$class="col-md-9";
							}

						?>
						<div class="<?php echo esc_attr($class); ?>">
							
							<div class="blog-box">
								<?php if($version=='boxed'){ ?>
									<div class="blog-title">
										<p> <?php the_time(get_option( 'date_format' )); ?> <?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?> <?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
										<h1><?php the_title(); ?></h1>
									</div>
								<?php } ?>
								<div class="blog-post single-post">
									<?php the_content(); ?>
									<?php
									$defaults = array(
									  'before'           => '<div id="page-links"><strong>Page: </strong>',
									  'after'            => '</div>',
									  'link_before'      => '<span>',
									  'link_after'       => '</span>',
									  'next_or_number'   => 'number',
									  'separator'        => ' ',
									  'nextpagelink'     => __( 'Next page','montblanc' ),
									  'previouspagelink' => __( 'Previous page','montblanc' ),
									  'pagelink'         => '%',
									  'echo'             => 1
									);
								   ?>
								  <?php wp_link_pages($defaults); ?>
								  <?php if(has_tag()){ ?>
									<?php the_tags('<ul class="single-post-tags"><li><span>Tags: </span> </li><li>',' </li>, <li>','</li></ul>'); ?>
								  <?php } ?>  
									<div class="share-box">
									  
									  <ul class="share-this-post">
									     <li class="facebook"><a onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook-square"></i></a></li>
							              <li class="twitter"><a  onclick="window.open('http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>"><i class="fa fa-twitter"></i></a></li>
							              <li  class="pinterest"><a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a></li>
							              <li  class="pinterest"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest"></i></a></li>
							              <li class="linkedin"><a onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"><i class="fa fa-linkedin"></i></a></li>
									  </ul>
									</div>

								</div>
								<div class="pre-next-post">
								  <div class="row">
								  	 <?php
						              $prev_post = get_adjacent_post(false, '', true);
						              $next_post = get_adjacent_post(false, '', false); 
						             ?>
						             
								    <!--=======  PREVIS POST =========-->
								    <div class="col-sm-6">
								    <?php if($prev_post!=null){ ?>
								    	<div class="row">
								    	 <div class="col-xs-3">
								    	 	<?php if(montblanc_thumbnail_url($size='', $id=$prev_post->ID)!=''){ ?>
								    	 		<img src="<?php echo bfi_thumb( montblanc_thumbnail_url($size='', $id=$prev_post->ID), array( 'width' => 70, 'height' => 70 ) ); ?>" class="pull-left" />
								    	 	<?php }else{ ?>
								    	 		<img  alt="<?php the_title(); ?>" src="http://placehold.it/70x70"  class="img-responsive" > 
								    	 	<?php } ?>
										      
										  </div>
									      <div class="prev-post col-xs-9">
									      <i class="fa fa-long-arrow-left"></i>  <span><?php esc_html_e('Previous Post','montblanc'); ?></span> 
									      <a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>"><?php echo esc_html($prev_post->post_title); ?> </a>
									      </div>
									  </div>
									<?php } ?>
								    </div>

								    <!--=======  NEXT POST =========-->
								    <div class="col-sm-6">
								    	<?php if($next_post!=null){ ?>
								    	<div class="row">
								     	<div class="next-post col-xs-9">
									        <span><?php esc_html_e('Next Post','montblanc'); ?></span> <i class="fa fa-long-arrow-right"></i> 
								        	<a href="<?php echo esc_url(get_permalink($next_post->ID)); ?>" title="<?php echo esc_attr($next_post->post_title); ?>"><?php echo esc_html($next_post->post_title); ?></a> 
								         </div>
								        <div class="col-xs-3">
								        <?php if(montblanc_thumbnail_url($size='', $id=$next_post->ID)!=''){ ?>
							    	 		<img src="<?php echo bfi_thumb( montblanc_thumbnail_url($size='', $id=$next_post->ID), array( 'width' => 70, 'height' => 70 ) ); ?>" class="pull-left" />
							    	 	<?php }else{ ?>
							    	 		<img  alt="<?php the_title(); ?>" src="http://placehold.it/70x70"  class="img-responsive" > 
							    	 	<?php } ?>
								        </div>	
								     </div>
								     <?php } ?>
								    </div>
								  </div>
								</div>

								<?php comments_template(); ?>
							</div>
						
						</div>
						<?php if($version!='boxed' and $version!='vertical'){ ?>
						<div class="col-md-3">
							<?php get_sidebar(); ?>
						</div>

						<?php } ?>

					</div>
				</div>
			</div>
			<?php } ?>
			<!-- End blog-section -->
			<?php endwhile; ?>
		</div>
		<!-- End content -->

<?php get_footer(); ?>