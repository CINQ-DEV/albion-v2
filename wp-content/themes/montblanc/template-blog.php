<?php 
/*
*Template Name: Blog List
*/
?>
<?php get_header(); ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}

?>
		<!-- content 
			================================================== -->
		<div id="content">
		<?php if($version!='vertical'){ ?>
			<!-- page-banner-section
				================================================== -->
			<div class=" page-banner-section blog-banner" <?php global $post; if(get_post_meta($post->ID, '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta($post->ID, '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">
					<div class="page-banner-box">
						<h1><?php single_post_title(); ?></h1>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->
			<?php } ?>
			<!-- blog-section
				================================================== -->
			<div class="section-content blog-section">
				<?php if($version!='vertical'){ ?>
				<div class="container">
					<div class="row">
				<?php } ?>
						<?php 
							if($version!='default'){
								get_template_part('blog', $version);
							}else{ 
						?>
						<div class="col-md-9">

							<div class="blog-box">

								<?php 
									if(is_front_page()) {
										$paged = (get_query_var('page')) ? get_query_var('page') : 1;
									} else {
										$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
									}
									$args = array(
										'post_type' => 'post',
										'posts_per_page' =>6,
										'paged' => $paged,
									);
									$query = new WP_Query($args);
								?>

								<?php 
								if($query->have_posts()) :
												while($query->have_posts()) : $query->the_post(); 
								?>
					                <?php get_template_part( 'content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>

					                <?php endwhile; ?>
					                <?php else: ?>
					                <div class="not-found">
					                    <h1><?php esc_html_e('Nothing Found Here!','montblanc'); ?></h1>
					                    <h3><?php esc_html_e('Search with other string:', 'montblanc') ?></h3>
					                    <div class="search-form">
					                        <?php get_search_form(); ?>
					                    </div>
					                </div>
					            <?php endif; ?>
					            <div class="pagination-list">
						            <?php montblanc_pagination($prev = '<i class="fa fa-long-arrow-left"></i>preious page', $next = 'next page<i class="fa fa-long-arrow-right"></i>', $pages=$query->max_num_pages); ?>
						        </div>
								
							</div>

						</div>

						<div class="col-md-3">
							<?php get_sidebar(); ?>
						</div>
						<?php } ?>
				<?php if($version!='vertical'){ ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<!-- End blog-section -->
			<?php 
				while(have_posts()) : the_post();
					the_content(); 
				endwhile;
			?>
		</div>
		<!-- End content -->

<?php get_footer(); ?>