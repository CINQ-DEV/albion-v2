<?php 
	global $montblanc_options;
?>
<!-- Header
    ================================================== -->
<header class="clearfix">
	<!-- Static navbar -->
	<div class="navbar navbar-default navbar-static-top">
		<div class="nav-wrap">
			<div class="container relative">
				<div class="navbar-header">
					
					<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
					<?php if($montblanc_options['logo']['url']!=''){ ?>
						<img src="<?php echo esc_attr($montblanc_options['logo']['url']); ?>" alt="<?php bloginfo('name'); ?>">
					<?php }else{ ?>
						<?php bloginfo('name'); ?>
					<?php } ?>
					</a>
				</div>
				<div class="">
					<a href="#" class="open-menu">
						<span></span>
						<span></span>
						<span></span>
						<span class="close-menu"></span>
					</a>
					<div class="site-nav">
						<div class="container">
							<?php if($montblanc_options['logo-alt']['url']!=''){ ?>
							<img src="<?php echo esc_attr($montblanc_options['logo-alt']['url']); ?>" alt="<?php bloginfo('name'); ?>">
							<?php } ?>
							<?php
						
								$defaults1= array(
									'theme_location'  => 'primary',
									'menu'            => '',
									'container'       => '',
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									 'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
									 //'walker'            => new wp_bootstrap_navwalker(),
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 0,
								);
								if ( has_nav_menu( 'primary' ) ) {
									wp_nav_menu( $defaults1 );
							} ?>
							

							<ul class="social_icons">
							   <?php if($montblanc_options['facebook']!=''){ ?>
							  <li class="facebook"><a  href="<?php echo esc_url($montblanc_options['facebook']); ?>"><i class="fa fa-facebook"></i></a></li>
							  <?php } ?>
							  <?php if($montblanc_options['twitter']!=''){ ?>
							  <li class="twitter"><a href="<?php echo esc_url($montblanc_options['twitter']); ?>"><i class="fa fa-twitter"></i></a></li>
							  <?php } ?>
							 <?php if($montblanc_options['istagram']!=''){ ?>
								<li class="instagram"><a href="<?php echo esc_url($montblanc_options['twitter']); ?>"><i class="fa fa-instagram"> </i></a></li>
								<?php } ?>
							  <?php if($montblanc_options['google']!=''){ ?>
							  <li class="google"><a  href="<?php echo esc_url($montblanc_options['google']); ?>"><i class="fa fa-google-plus"></i></a></li>
							  <?php } ?>
							  <?php if($montblanc_options['linkedin']!=''){ ?>
							  <li class="linkedin"><a  href="<?php echo esc_url($montblanc_options['linkedin']); ?>"><i class="fa fa-linkedin"></i></a></li>
							  <?php } ?>
							  <?php if($montblanc_options['pinterest']!=''){ ?>
							  <li class="pinterest" ><a href="<?php echo esc_url($montblanc_options['pinterest']); ?>"><i class="fa fa-pinterest"></i></a></li>
							  <?php } ?>
							</ul>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- End Header -->