<?php 

  


  if(function_exists('vc_map')){


    add_action( 'init', 'portfolio_tax1', 999999 );
    function portfolio_tax1()
    {
    $categories = get_terms('portfolio_category' , 'hide_empty=0');
    
    $category_option = array();
    $category_option['All'] = 'all';
    foreach ($categories as $category) {

    $category_option[$category->name] = $category->slug;

    }

    $args = array(
	  'posts_per_page'   => -1,
	  'offset'           => 0,
	  'category'         => '',
	  'category_name'    => '',
	  'orderby'          => 'post_title',
	  'order'            => 'ASC',
	  'include'          => '',
	  'exclude'          => '',
	  'meta_key'         => '',
	  'meta_value'       => '',
	  'post_type'        => 'page',
	  'post_mime_type'   => '',
	  'post_parent'      => '',
	  'post_status'      => 'publish',
	  'suppress_filters' => true 
	);
	$pages = get_posts( $args );
	$lists_page = array();
	foreach($pages as $p){
	  $lists_page[$p->post_title] = $p->ID;
	}

    vc_map( array(
       "name" => esc_html__("QK Portfolio","montblanc"),
       "base" => "qk_portfolio_category",
       "class" => "",
       "category" => esc_html__("Content", "montblanc"),
       "icon" => "icon-qk",
       "params" => array(
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Select Category","montblanc"),
         "param_name" => "cat",
         "value" => $category_option,
         "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Order","montblanc"),
             "param_name" => "order",
             "value" => "",
             "description" => 'Example "12"'
          ),
           array(
             "type" => "dropdown",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__( "Show Filter","montblanc" ),
             "param_name" => "filter",
             "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
             "std" => 3,
             "description" => ""
           ), array(
             "type" => "dropdown",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__( "Show View All Button","montblanc" ),
             "param_name" => "view",
             "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
             "std" => 3,
             "description" => ""
           ),
             array(
              "type" => "dropdown",
              "holder" => "div",
              "class" => "",
              "heading" => esc_html__("View All link","montblanc"),
              "param_name" => "btn_link",
              "value" => $lists_page,
              "description" => ''
            ),
	      
       )
    ) );
  }

  }
  class WPBakeryShortCode_qk_portfolio_category extends WPBakeryShortCode {
  }





  if(function_exists('vc_map')){


    add_action( 'init', 'portfolio_tax2', 999999 );
    function portfolio_tax2()
    {
    $categories = get_terms('portfolio_category' , 'hide_empty=0');
   
    $category_option = array();
    $category_option['All'] = 'all';
    foreach ($categories as $category) {

    $category_option[$category->name] = $category->slug;

    }
    
    $args = array(
	  'posts_per_page'   => -1,
	  'offset'           => 0,
	  'category'         => '',
	  'category_name'    => '',
	  'orderby'          => 'post_title',
	  'order'            => 'ASC',
	  'include'          => '',
	  'exclude'          => '',
	  'meta_key'         => '',
	  'meta_value'       => '',
	  'post_type'        => 'page',
	  'post_mime_type'   => '',
	  'post_parent'      => '',
	  'post_status'      => 'publish',
	  'suppress_filters' => true 
	);
	$pages = get_posts( $args );
	$lists_page = array();
	foreach($pages as $p){
	  $lists_page[$p->post_title] = $p->ID;
	}


  global $montblanc_options;
  if($montblanc_options!=null && $montblanc_options['body_style']!=''){
    $version = $montblanc_options['body_style'];
  }else{
    $version = 'default';
  }

  if($version!='vertical'){
    vc_map( array(
       "name" => esc_html__("QK Portfolio 3Col","montblanc"),
       "base" => "qk_portfolio_category2",
       "class" => "",
       "category" => esc_html__("Content", "montblanc"),
       "icon" => "icon-qk",
       "params" => array(
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Select Category","montblanc"),
         "param_name" => "cat",
         "value" => $category_option,
         "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Order","montblanc"),
             "param_name" => "order",
             "value" => "",
             "description" => 'Example "12"'
          ),
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__( "Show Filter","montblanc" ),
         "param_name" => "filter",
         "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
         "std" => 3,
         "description" => ""
       ), array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__( "Show View All Button","montblanc" ),
         "param_name" => "view",
         "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
         "std" => 3,
         "description" => ""
       ),
         array(
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_html__("View All link","montblanc"),
          "param_name" => "btn_link",
          "value" => $lists_page,
          "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Extra class","montblanc"),
             "param_name" => "ex_class",
             "value" => "",
             "description" => 'Example "no-padd"'
          ),
       )
    ) );
  }

    
  }

  }
  class WPBakeryShortCode_qk_portfolio_category2 extends WPBakeryShortCode {
  }




  if(function_exists('vc_map')){


    add_action( 'init', 'portfolio_tax3', 999999 );
    function portfolio_tax3()
    {
    $categories = get_terms('portfolio_category' , 'hide_empty=0');
    
    $category_option = array();
    $category_option['All'] = 'all';
    foreach ($categories as $category) {

    
    $category_option[$category->name] = $category->slug;

    }
    
    $args = array(
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'post_title',
    'order'            => 'ASC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'page',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true 
  );
  $pages = get_posts( $args );
  $lists_page = array();
  foreach($pages as $p){
    $lists_page[$p->post_title] = $p->ID;
  }

    vc_map( array(
       "name" => esc_html__("QK Portfolio ALT","montblanc"),
       "base" => "qk_portfolio_category3",
       "class" => "",
       "category" => esc_html__("Content", "montblanc"),
       "icon" => "icon-qk",
       "params" => array(
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Select Category","montblanc"),
         "param_name" => "cat",
         "value" => $category_option,
         "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Order","montblanc"),
             "param_name" => "order",
             "value" => "",
             "description" => 'Example "12"'
          ),
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__( "Show Filter","montblanc" ),
         "param_name" => "filter",
         "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
         "std" => 3,
         "description" => ""
       ), array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__( "Show View All Button","montblanc" ),
         "param_name" => "view",
         "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
         "std" => 3,
         "description" => ""
       ),
         array(
          "type" => "dropdown",
          "holder" => "div",
          "class" => "",
          "heading" => esc_html__("View All link","montblanc"),
          "param_name" => "btn_link",
          "value" => $lists_page,
          "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Extra class","montblanc"),
             "param_name" => "ex_class",
             "value" => "",
             "description" => 'Example "no-padd"'
          ),
       )
    ) );
  }

  }
  class WPBakeryShortCode_qk_portfolio_category3 extends WPBakeryShortCode {
  }


//QK Flexslider
vc_map( array(
    "name" => esc_html__("QK Team Wrapper", "montblanc"),
    "base" => "qk_team_wrap",
    "as_parent" => array('only' => 'qk_team'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_montblanc" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content montblanc
        array(
            "type" => "textfield",
            "heading" => esc_html__("Class", "montblanc"),
            "param_name" => "class",
            "description" => 'example "bnr-slide-2"',
        )
    ),
    "js_view" => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_qk_team_wrap extends WPBakeryShortCodesContainer {
    }
}



if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Team","montblanc"),
   "base" => "qk_team",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "content_montblanc" => true,
   "as_child" => array('only' => 'qk_team_wrap'),
   "params" => array(
   
      
     array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Avatar","montblanc"),
         "param_name" => "image",
         "value" => "",
         "description" => 'Your member avatar'
      ), array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Name","montblanc"),
         "param_name" => "name",
         "value" => "",
         "description" => 'Your member name'
      ), array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Job","montblanc"),
         "param_name" => "job",
         "value" => "",
         "description" => 'Your member Job'
      ),array(
         "type" => "textarea",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Socials","montblanc"),
         "param_name" => "content",
         "value" => "",
         "description" => 'Your member socials'
      )
   )
) );

}
class WPBakeryShortCode_qk_team extends WPBakeryShortCode {
}



//Service

if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Skill","montblanc"),
   "base" => "qk_skill",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "params" => array(
   
     array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title","montblanc"),
         "param_name" => "title",
         "value" => "",
         "description" => 'Your skill title'
      ),
    
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Level","montblanc"),
         "param_name" => "level",
         "value" => "",
         "description" => 'Your level example "70"'
      ),
    
   )
) );

}
class WPBakeryShortCode_qk_skill extends WPBakeryShortCode {
}



//Service

vc_map( array(
    "name" => esc_html__("QK Statistic Wrapper", "montblanc"),
    "base" => "qk_statistic_wrap",
    "as_parent" => array('only' => 'qk_statistic'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_montblanc" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content montblanc
        array(
            "type" => "textfield",
            "heading" => esc_html__("Class", "montblanc"),
            "param_name" => "class",
            "description" => '',
        )
    ),
    "js_view" => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_qk_statistic_wrap extends WPBakeryShortCodesContainer {
    }
}

if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Statistic","montblanc"),
   "base" => "qk_statistic",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "content_montblanc" => true,
   "as_child" => array('only' => 'qk_statistic_wrap'),
   "icon" => "icon-qk",
   "params" => array(
   
     array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title","montblanc"),
         "param_name" => "title",
         "value" => "",
         "description" => 'Your  title'
      ),
    
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Number","montblanc"),
         "param_name" => "level",
         "value" => "",
         "description" => 'Your number counter'
      ),
    
   )
) );

}
class WPBakeryShortCode_qk_statistic extends WPBakeryShortCode {
}

vc_map( array(
    "name" => esc_html__("QK Clients Wrapper", "montblanc"),
    "base" => "qk_clients_wrap",
    "as_parent" => array('only' => 'qk_client'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_montblanc" => true,
    "show_settings_on_create" => false,
    "params" => array(
        // add params same as with any other content montblanc
        array(
            "type" => "textfield",
            "heading" => esc_html__("Class", "montblanc"),
            "param_name" => "class",
            "description" => '',
        )
    ),
    "js_view" => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_qk_clients_wrap extends WPBakeryShortCodesContainer {
    }
}



if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Client","montblanc"),
   "base" => "qk_client",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "content_montblanc" => true,
   "as_child" => array('only' => 'qk_clients_wrap'),
   "params" => array(
   
      
     array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Client Logo","montblanc"),
         "param_name" => "image",
         "value" => "",
         "description" => 'Your client Logo'
      ), array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("client Link","montblanc"),
         "param_name" => "link",
         "value" => "",
         "description" => 'Your client link'
      )
   )
) );

}
class WPBakeryShortCode_qk_client extends WPBakeryShortCode {
}
//Service

if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Service Post","montblanc"),
   "base" => "qk_service",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "params" => array(
   
     array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title","montblanc"),
         "param_name" => "title",
         "value" => "",
         "description" => ''
      ),
    
     
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Service icon","montblanc"),
         "param_name" => "icon",
         "value" => "",
         "description" => 'Font Awesome Icon Class'
      ),
    array(
         "type" => "textarea",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Content ","montblanc"),
         "param_name" => "content",
         "value" => '',
         "description" => ''
      ),
   )
) );

}
class WPBakeryShortCode_qk_service extends WPBakeryShortCode {
}


if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Gmap","montblanc"),
   "base" => "qk_gmap",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "params" => array(
   
      
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Latitude","montblanc"),
         "param_name" => "lat",
         "value" => "",
         "description" => 'Example "45.2512517"'
      ),
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Longitude","montblanc"),
         "param_name" => "lon",
         "value" => "",
         "description" => 'Example "12.2210728"'
      ),
    array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Map Height","montblanc"),
         "param_name" => "height",
         "value" => "",
         "description" => 'Example "500px"'
      ),
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Map marker","montblanc"),
         "param_name" => "image",
         "value" => "",
         "description" => ''
      )
   
   )
) );

}
class WPBakeryShortCode_qk_gmap extends WPBakeryShortCode {
}



  if(function_exists('vc_map')){


    add_action( 'init', 'lookbook_tax1', 999999 );
    function lookbook_tax1()
    {
    $categories = get_terms('lookbook_category' , 'hide_empty=0');
   
    $category_option = array();
    $category_option['All'] = 'all';
    foreach ($categories as $category) {

    
    $category_option[$category->name] = $category->slug;

    }

    $args = array(
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'post_title',
    'order'            => 'ASC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'page',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true 
  );
  $pages = get_posts( $args );
  $lists_page = array();
  foreach($pages as $p){
    $lists_page[$p->post_title] = $p->ID;
  }

    vc_map( array(
       "name" => esc_html__("QK Lookbook","montblanc"),
       "base" => "qk_lookbook",
       "class" => "",
       "category" => esc_html__("Content", "montblanc"),
       "icon" => "icon-qk",
       "params" => array(
          array(
         "type" => "dropdown",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Select Category","montblanc"),
         "param_name" => "cat",
         "value" => $category_option,
         "description" => ''
        ),
         array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__("Order","montblanc"),
             "param_name" => "order",
             "value" => "",
             "description" => 'Example "12"'
          ),
           array(
             "type" => "dropdown",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__( "Show Filter","montblanc" ),
             "param_name" => "filter",
             "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
             "std" => 3,
             "description" => ""
           ), array(
             "type" => "dropdown",
             "holder" => "div",
             "class" => "",
             "heading" => esc_html__( "Show View All Button","montblanc" ),
             "param_name" => "view",
             "value" => array( "Select"=>"slect", "Yes" =>"yes", "No"=>"no" ),
             "std" => 3,
             "description" => ""
           ),
             array(
              "type" => "dropdown",
              "holder" => "div",
              "class" => "",
              "heading" => esc_html__("View All link","montblanc"),
              "param_name" => "btn_link",
              "value" => $lists_page,
              "description" => ''
            ),
        
       )
    ) );
  }

  }
  class WPBakeryShortCode_qk_lookbook extends WPBakeryShortCode {
  }



if(function_exists('vc_map')){

vc_map( array(
   "name" => esc_html__("QK Shop Banner","montblanc"),
   "base" => "qk_banner",
   "class" => "",
   "category" => esc_html__("Content", "montblanc"),
   "icon" => "icon-qk",
   "content_montblanc" => true,
   "params" => array(
   
      
     array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Banner image","montblanc"),
         "param_name" => "image",
         "value" => "",
         "description" => 'Upload your banner image'
      ), array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Title","montblanc"),
         "param_name" => "name",
         "value" => "",
         "description" => 'Your banner title'
      ), array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Link","montblanc"),
         "param_name" => "url",
         "value" => "",
         "description" => 'Your banner url'
      )
   )
) );

}
class WPBakeryShortCode_qk_banner extends WPBakeryShortCode {
}

?>