<div class="col-md-12">

	<div class="blog-box">

		<?php 
			if(is_front_page()) {
				$paged = (get_query_var('page')) ? get_query_var('page') : 1;
			} else {
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			}
			$args = array(
				'post_type' => 'post',
				'posts_per_page' =>5,
				'paged' => $paged,
			);
			$query = new WP_Query($args);
		?>
		<?php 
		if($query->have_posts()) :
						while($query->have_posts()) : $query->the_post(); 
		?>
		<?php 
			$bg = montblanc_thumbnail_url('','');
		?>
		<div class="blog-post" <?php if($bg!=''){  ?>style="background-image: url(<?php echo esc_attr($bg); ?>);" <?php } ?>>
			
			<div class="blog-content">
				<div class="blog-title">
					<p class="post-meta"> <?php the_time(get_option( 'date_format' )); ?><?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?></p>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<p class="post-comments"><?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
				</div>
				
			</div>
		</div>
		<?php endwhile; endif; ?>
		

		<div class="pagination-list">
            <?php montblanc_pagination($prev = '<i class="fa fa-long-arrow-left"></i>preious page', $next = 'next page<i class="fa fa-long-arrow-right"></i>', $pages=$query->max_num_pages); ?>
        </div>
	</div>

</div>