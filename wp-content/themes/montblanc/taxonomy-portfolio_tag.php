<?php get_header(); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- page-banner-section
				================================================== -->
			<div class="section-content page-banner-section portfolio-banner">
				<div class="container">
					<div class="page-banner-box">
						<h1>Portfolio: <?php single_term_title() ?></h1>
						<p>- Check our portfolio</p>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->

			<!-- portfolio-section
				================================================== -->
			<div class="section-content portfolio-section grid-style-fullwidth">
				

				<div class="portfolio-box masonry">

					<?php  while(have_posts()) : the_post(); ?>
					<?php
						$item_classes = '';
						$item_skill = '';
						$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
						foreach((array)$item_cats as $item_cat){
							if(count($item_cat)>0){
								$item_classes .= $item_cat->slug . ' ';
								
							}
						}
					?>
					

					<div class="project-post <?php echo esc_attr($item_classes); ?>">
						<?php
		                    if(has_post_thumbnail()){
		                    // Get the URL of our processed image
		                    $image = bfi_thumb( montblanc_thumbnail_url('',''), array( 'width' => 450, 'height' => 360 ) );
		                ?>
		                <img  alt="<?php the_title(); ?>" src="<?php echo esc_attr($image); ?>"   > 
		                
		                <?php
		                    }else{
		                ?><img  alt="<?php the_title(); ?>" src="http://placehold.it/450x360"   > 
		                <?php } ?>
						<div class="hover-box">
							<div class="inner-hover">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php 
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
										$i=1; foreach((array)$item_cats as $item_cat){
											if(count($item_cat)>0){
												
												if($i==1){
													$item_skill .= $item_cat->name;
												}else{
													$item_skill .= $item_cat->name . ' , ';
												}
												
											}
									$i++; }
									echo esc_html($item_skill);
								?></p>
							</div>
						</div>
					</div>

					<?php endwhile; ?>

				</div>

				<div class="center-button">
					<a class="button-one" href="portfolio.html">Show more</a>
				</div>

			</div>
			<!-- End portfolio section -->

		</div>
		<!-- End content -->
<?php get_footer(); ?>