<?php 
/*
*Template Name: Portfolio List
*/
?>
<?php get_header(); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- page-banner-section
				================================================== -->
			<div class="page-banner-section portfolio-banner" <?php global $post; if(get_post_meta($post->ID, '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta($post->ID, '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">
					<div class="page-banner-box">
						<h1><?php single_post_title(); ?></h1>
						<?php if(get_post_meta($post->ID, '_cmb_p_subtitle', true)!=''){ ?>
						<p>- <?php echo get_post_meta($post->ID, '_cmb_p_subtitle', true); ?></p>
						<?php } ?>
						<?php 

						global $montblanc_options;
						if($montblanc_options['body_style']!=''){
							$version = $montblanc_options['body_style'];
						}else{
							$version = 'default';
						}
							
						?>
						<?php if($version!='boxed'){ ?>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->
			<?php 

			global $montblanc_options;
			if($montblanc_options['body_style']!=''){
				$version = $montblanc_options['body_style'];
			}else{
				$version = 'default';
			}
				
			?>
			<?php if($version=='boxed'){ ?>

			<div class="section-content portfolio-section masonry-style no-padding-top">
				<div class="container">
					<ul class="filter">
					<?php $portfolio_skills = get_terms('portfolio_tag'); ?>
					<li><a href="#" class="active" data-filter="*">All <span>[<?php echo esc_html( 9);?>]</span></a></li>
					<?php foreach($portfolio_skills as $portfolio_skill) { ?>
						<?php 
						if(isset($atts['cat']) and $atts['cat']!='all'){
							$query = new WP_Query( 

								array('post_type' => 'portfolio', 'posts_per_page' => 9, 'tax_query' => array(
									array(
										'taxonomy' => 'portfolio_category',
										'field'    => 'slug',
										'terms'    => $atts['cat'],
									),
								))

							);
						}else{
							$query = new WP_Query( 

								array('post_type' => 'portfolio', 'posts_per_page' => 9, 'tax_query' => array(
									
									
								))

							);
						}
						 
						 $count = 0;
						?>
						<?php  while($query->have_posts()) : $query->the_post(); ?>
							
							<?php
								$item_classes = '';
								$item_skill = '';
								$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
								foreach((array)$item_cats as $item_cat){
									if(count($item_cat)>0){
										$item_classes = $item_cat->slug;

										if($item_classes==$portfolio_skill->slug){
											$count = $count+1;
										}
										
									}
								}
							?>


						<?php endwhile; ?>
						
						<?php if($count>=1){ ?>
						<li><a href="#" data-filter=".<?php echo esc_attr($portfolio_skill->slug); ?>"><?php echo esc_html($portfolio_skill->name); ?> <span>[<?php echo esc_html($count); ?>]</span></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
				<div class="portfolio-box masonry ">
				<?php 
						if(is_front_page()) {
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						} else {
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						}
						$args = array(
							'post_type' => 'portfolio',
							'posts_per_page' =>9,
							'paged' => $paged,
						);
						$portfolio = new WP_Query($args);
					?>
				<?php  while($portfolio->have_posts()) : $portfolio->the_post(); ?>
				<?php
					$item_classes = '';
					$item_skill = '';
					$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
					foreach((array)$item_cats as $item_cat){
						if(count($item_cat)>0){
							$item_classes .= $item_cat->slug . ' ';
							
						}
					}
				?>

				<div class="project-post <?php echo esc_attr($item_classes); ?>">
					<?php the_post_thumbnail(); ?>
					<div class="hover-box">
						<div class="inner-hover">
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<p><?php 
								$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
									$i=1; foreach((array)$item_cats as $item_cat){
										if(count($item_cat)>0){
											
											if($i==1){
												$item_skill .= $item_cat->name;
											}else{
												$item_skill .= $item_cat->name . ' , ';
											}
											
										}
								$i++; }
								echo esc_html($item_skill);
							?></p>
						</div>
					</div>
				</div>
			<?php endwhile;?>
			
				</div>
				<div class="pagination-list top-60">
		            <?php montblanc_pagination($prev = '<i class="fa fa-long-arrow-left"></i>preious page', $next = 'next page<i class="fa fa-long-arrow-right"></i>', $pages=$portfolio->max_num_pages); ?>
		        </div>
				</div>
			</div>

			<?php }else{ ?>
			<!-- portfolio-section
				================================================== -->
			<div class="section-content portfolio-section grid-style-fullwidth">
				<div class="container">

					<ul class="filter">
						<?php $portfolio_skills = get_terms('portfolio_tag'); ?>
						<li><a href="#" class="active" data-filter="*">All </a></li>
						<?php foreach($portfolio_skills as $portfolio_skill) { ?>
							<li><a href="#" data-filter=".<?php echo esc_attr($portfolio_skill->slug); ?>"><?php echo esc_html($portfolio_skill->name); ?> <span>[<?php echo esc_html($portfolio_skill->count); ?>]</span></a></li>
						<?php } ?>
					</ul>

				</div>

				<div class="portfolio-box masonry">

					<?php 
						if(is_front_page()) {
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						} else {
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						}
						$args = array(
							'post_type' => 'portfolio',
							'posts_per_page' =>12,
							'paged' => $paged,
						);
						$query = new WP_Query($args);
					?>

					<?php  while($query->have_posts()) : $query->the_post(); ?>
					<?php
						$item_classes = '';
						$item_skill = '';
						$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
						foreach((array)$item_cats as $item_cat){
							if(count($item_cat)>0){
								$item_classes .= $item_cat->slug . ' ';
								
							}
						}
					?>
					

					<div class="project-post <?php echo esc_attr($item_classes); ?>">
						<?php
		                    if(has_post_thumbnail()){
		                    // Get the URL of our processed image
		                    $image = bfi_thumb( montblanc_thumbnail_url('',''), array( 'width' => 450, 'height' => 360 ) );
		                ?>
		                <img  alt="<?php the_title(); ?>" src="<?php echo esc_attr($image); ?>"   > 
		                
		                <?php
		                    }else{
		                ?><img  alt="<?php the_title(); ?>" src="http://placehold.it/450x360"   > 
		                <?php } ?>
						<div class="hover-box">
							<div class="inner-hover">
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php 
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
										$i=1; foreach((array)$item_cats as $item_cat){
											if(count($item_cat)>0){
												
												if($i==1){
													$item_skill .= $item_cat->name;
												}else{
													$item_skill .= $item_cat->name . ' , ';
												}
												
											}
									$i++; }
									echo esc_html($item_skill);
								?></p>
							</div>
						</div>
					</div>

					<?php endwhile; ?>

				</div>
				<div class="container-fluid">
				<div class="pagination-list top-60">
		            <?php montblanc_pagination($prev = '<i class="fa fa-long-arrow-left"></i>preious page', $next = 'next page<i class="fa fa-long-arrow-right"></i>', $pages=$query->max_num_pages); ?>
		        </div>
		    </div>
			</div>
			<!-- End portfolio section -->
			<?php } ?>
		</div>
		<!-- End content -->
<?php get_footer(); ?>