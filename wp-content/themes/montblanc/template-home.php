<?php 
/*
*Template Name: Home Builder
*/
?>
<?php get_header(); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<?php 
				while(have_posts()) : the_post();
					the_content();
				endwhile;
			?>

		</div>
		<!-- End content -->
<?php get_footer(); ?>