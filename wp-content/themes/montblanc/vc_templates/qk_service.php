<?php
$css = '';
extract(shortcode_atts(array(
  'icon' => '',
  'title' => '',
  'link' => '',
), $atts));

?>

<div class="services-post">
	<i class="fa <?php echo esc_attr($icon); ?>"></i>
	<div class="post-content">
		<h2><?php echo esc_html($title); ?></h2>
		<?php echo wpb_js_remove_wpautop($content); ?>
	</div>
</div>
