<?php
extract(shortcode_atts(array(
	
	'class'=>'',
), $atts));

?>

<div class="team-section <?php echo esc_attr($class); ?>" >
		<?php echo wpb_js_remove_wpautop($content); ?>
</div>
