<?php
$css = '';
extract(shortcode_atts(array(
  
  'title' => '',
  'level' => '',
), $atts));

?>
<div class="skill-post">
<p><?php echo esc_html($title); ?></p>
<div class="meter nostrips ">
	<p style="width: <?php echo esc_attr($level); ?>%"><span><?php echo esc_html($level); ?>%</span></p>
</div>
</div>