<?php
extract(shortcode_atts(array(
	
	'class'=>'',
), $atts));

?>

<div class="bxslider-clients <?php echo esc_attr($class); ?>" >
		<?php echo wpb_js_remove_wpautop($content); ?>
</div>
