<?php
$css = '';
extract(shortcode_atts(array(
  
  'title' => '',
  'level' => '',
), $atts));

?>

<div class="statistic-post">
	<p><span class="timer" data-from="0" data-to="<?php echo esc_attr($level); ?>"></span></p>
	<h2><?php echo esc_html($title); ?></h2>
</div>
