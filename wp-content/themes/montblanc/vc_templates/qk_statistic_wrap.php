<?php
extract(shortcode_atts(array(
	
	'class'=>'',
), $atts));
wp_enqueue_script("countTo", get_template_directory_uri()."/js/jquery.countTo.js",array(),false,true);
?>

<div class="statistic-box <?php echo esc_attr($class); ?>" >
		<?php echo wpb_js_remove_wpautop($content); ?>
</div>
