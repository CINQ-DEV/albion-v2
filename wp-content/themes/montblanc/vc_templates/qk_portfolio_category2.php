<?php
$css = '';
extract(shortcode_atts(array(
    'order' => '',
    'cat' => '',
    'btn_link' => '',
    'filter' => '',
    'view' => '',
    'ex_class' => '',
), $atts));
if(isset($atts['cat']) and $atts['cat']!='all'){
	$args = array('post_type' => 'portfolio', 'posts_per_page' => $atts['order'], 'tax_query' => array(
	array(
		'taxonomy' => 'portfolio_category',
		'field'    => 'slug',
		'terms'    => $atts['cat'],
	),
));
}else{
	$args = array('post_type' => 'portfolio', 'posts_per_page' => $atts['order']);
}	
$portfolio = new WP_Query($args);
?>

<!-- portfolio-section
	================================================== -->
<div class="section-content portfolio-section masonry-style no-padding-top">
	<div class="container">
		<?php if($filter!='no'){ ?>
		<ul class="filter">
			<?php $portfolio_skills = get_terms('portfolio_tag'); ?>
			<li><a href="#" class="active" data-filter="*">All <span>[<?php echo esc_html( $atts['order']);?>]</span></a></li>
			<?php foreach($portfolio_skills as $portfolio_skill) { ?>
				<?php 
				if(isset($atts['cat']) and $atts['cat']!='all'){
					$query = new WP_Query( 

						array('post_type' => 'portfolio', 'posts_per_page' => $atts['order'], 'tax_query' => array(
							array(
								'taxonomy' => 'portfolio_category',
								'field'    => 'slug',
								'terms'    => $atts['cat'],
							),
						))

					);
				}else{
					$query = new WP_Query( 

						array('post_type' => 'portfolio', 'posts_per_page' => $atts['order'], 'tax_query' => array(
							
							
						))

					);
				}
				 
				 $count = 0;
				?>
				<?php  while($query->have_posts()) : $query->the_post(); ?>
					
					<?php
						$item_classes = '';
						$item_skill = '';
						$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
						foreach((array)$item_cats as $item_cat){
							if(count($item_cat)>0){
								$item_classes = $item_cat->slug;

								if($item_classes==$portfolio_skill->slug){
									$count = $count+1;
								}
								
							}
						}
					?>


				<?php endwhile; ?>
				
				<?php if($count>=1){ ?>
				<li><a href="#" data-filter=".<?php echo esc_attr($portfolio_skill->slug); ?>"><?php echo esc_html($portfolio_skill->name); ?> <span>[<?php echo esc_html($count); ?>]</span></a></li>
				<?php } ?>
			<?php } ?>
		</ul>
		<?php } ?>
	
		<div class="portfolio-box masonry <?php echo esc_attr($ex_class); ?>">
			<?php  while($portfolio->have_posts()) : $portfolio->the_post(); ?>
			<?php
				$item_classes = '';
				$item_skill = '';
				$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
				foreach((array)$item_cats as $item_cat){
					if(count($item_cat)>0){
						$item_classes .= $item_cat->slug . ' ';
						
					}
				}
			?>
			<?php 

			global $montblanc_options;
			if($montblanc_options['body_style']!=''){
				$version = $montblanc_options['body_style'];
			}else{
				$version = 'default';
			}
				
			?>
			<?php if($version=='boxed'){ ?>

			<div class="project-post <?php echo esc_attr($item_classes); ?>">
				<?php the_post_thumbnail(); ?>
				<div class="hover-box">
					<div class="inner-hover">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<p><?php 
							$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
								$i=1; foreach((array)$item_cats as $item_cat){
									if(count($item_cat)>0){
										
										if($i==1){
											$item_skill .= $item_cat->name;
										}else{
											$item_skill .= $item_cat->name . ' , ';
										}
										
									}
							$i++; }
							echo esc_html($item_skill);
						?></p>
					</div>
				</div>
			</div>

			
			<?php }else{ ?>
			<div class="project-post <?php echo esc_attr($item_classes); ?>">
				<?php the_post_thumbnail(); ?>
				<div class="hover-box">
					<div class="inner-hover">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<p><?php echo get_post_meta(get_the_ID(), '_cmb_p_subtitle', true); ?></p>
					</div>
					<span class="left-top corner-border"></span>
					<span class="left-bottom corner-border"></span>
					<span class="right-top corner-border"></span>
					<span class="right-bottom corner-border"></span>
				</div>
			</div>
			<?php } ?>
			<?php endwhile; ?>
		</div>
		<?php if($view!='no'){ ?>
		<div class="center-button">
			<a class="button-one" href="<?php echo esc_url( get_permalink( $btn_link ) );?>">View All</a>
		</div>
		<?php } ?>
		</div>
</div>
<!-- End portfolio section -->

