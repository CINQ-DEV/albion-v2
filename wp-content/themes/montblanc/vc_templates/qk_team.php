<?php
$css = '';
extract(shortcode_atts(array(
    
    'image' => '',
    'name' => '',
    'job' => '',
), $atts));

?>

<div class="team-post">
  <?php
    $img = wp_get_attachment_image_src($image,'full');
    $img = $img[0];
  ?>
  <img src="<?php echo esc_attr($img); ?>" alt="Slide" class="img-responsive">
  <div class="team-content">
    <h2><?php echo esc_html($name); ?></h2>
    <span><?php echo esc_html($job); ?></span>
    <ul class="social-team">
      <?php echo wpb_js_remove_wpautop($content); ?>
    </ul>
  </div>
</div>

