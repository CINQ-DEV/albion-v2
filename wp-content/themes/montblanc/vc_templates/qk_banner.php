<?php
$css = '';
extract(shortcode_atts(array(
    
    'image' => '',
    'name' => '',
    'url' => '',
    
), $atts));

?>

<div class="banner-post">
  <?php
    $img = wp_get_attachment_image_src($image,'full');
    $img = $img[0];
  ?>
  <img src="<?php echo esc_attr($img); ?>" alt="Slide" class="img-responsive">
  <div class="hover-box">
   
    <div class="inner-hover">
      <h2><a href="<?php echo esc_url($url); ?>"><?php echo esc_html($name); ?></a></h2>
      
    </div>
  </div>
</div>

