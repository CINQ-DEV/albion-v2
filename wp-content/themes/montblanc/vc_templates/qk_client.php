<?php
$css = '';
extract(shortcode_atts(array(
    
    'image' => '',
    'link' => '',
    'job' => '',
), $atts));

?>
<div class="slide"><a target="_blank" href="<?php echo esc_url($link); ?>"><?php
    $img = wp_get_attachment_image_src($image,'full');
    $img = $img[0];
  ?>
  <img src="<?php echo esc_attr($img); ?>" alt="Client" class="img-responsive"></a></div>

