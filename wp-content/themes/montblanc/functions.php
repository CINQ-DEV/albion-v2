<?php
require_once get_template_directory() . '/framework/theme-configs.php';
require_once dirname( __FILE__ ) . '/framework/Custom-Metaboxes/metabox-functions.php';
require_once get_template_directory() . '/framework/BFI_Thumb.php';
require_once get_template_directory() . '/framework/widget/popular.php';
require_once get_template_directory() . '/framework/wp_bootstrap_navwalker.php';
require_once get_template_directory() . '/framework/wp_bootstrap_navwalker2.php';
if(function_exists('vc_add_param')){
if ( is_plugin_active( 'qk-post_type/post_type.php' ) ) {
require_once get_template_directory() . '/vc_functions.php';
}
}

if ( ! isset( $content_width ) )
	$content_width = 604;

function montblanc_setup() {
	/*
	 * Makes montblanc available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on montblanc, use a find and
	 * replace to change 'montblanc' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'montblanc', get_template_directory() . '/languages' );

	
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	 add_theme_support( 'custom-header');
	 add_theme_support( 'custom-background');
	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	 add_theme_support( 'post-formats', array(
		 'video','gallery','audio'
	) );
	 
	add_theme_support( 'woocommerce' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', esc_html__( 'Navigation Menu', 'montblanc' ) );
	

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	
	
	
}
add_action( 'after_setup_theme', 'montblanc_setup' );

function montblanc_scripts_styles() {
	global $montblanc_options;
	if($montblanc_options['body_style']!=''){
		$version = $montblanc_options['body_style'];
	}else{
		$version = 'default';
	}
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Adds Masonry to handle vertical alignment of footer widgets.
	if ( is_active_sidebar( 'sidebar-1' ) )
		wp_enqueue_script( 'jquery-masonry' );
	
	
	// Loads JavaScript file with functionality specific to Twenty Thirteen.
	wp_enqueue_script("montblanc-bootstrap", get_template_directory_uri()."/js/bootstrap.js",array(),false,true);
    wp_enqueue_script("montblanc-appear", get_template_directory_uri()."/js/jquery.appear.js",array(),false,true);
    wp_enqueue_script("montblanc-imagesloaded", get_template_directory_uri()."/js/jquery.imagesloaded.min.js",array(),false,true);
    wp_enqueue_script("montblanc-isotope", get_template_directory_uri()."/js/jquery.isotope.min.js",array(),false,true);
    wp_enqueue_script("montblanc-waypoint", get_template_directory_uri()."/js/waypoint.min.js",array(),false,true);
    wp_enqueue_script("montblanc-retina", get_template_directory_uri()."/js/retina-1.1.0.min.js",array(),false,true);
    wp_enqueue_script("montblanc-scroll", get_template_directory_uri()."/js/plugins-scroll.js",array(),false,true);

    if($version!='boxed' and $version!='vertical'){
        wp_enqueue_script("smooth", get_template_directory_uri()."/js/smooth-scroll.js",array(),false,true);
    }
    
    wp_enqueue_script("montblanc-flexslider", get_template_directory_uri()."/js/jquery.flexslider.js",array(),false,true);
	wp_enqueue_script("montblanc-bxslider", get_template_directory_uri()."/js/jquery.bxslider.min.js",array(),false,true);
	
	wp_enqueue_script("theme-script", get_template_directory_uri()."/js/".$version."-script.js",array(),false,true);

	// Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
	wp_enqueue_style( 'montblanc-font-awesome-theme', get_template_directory_uri().'/css/font-awesome.css');
    wp_enqueue_style( 'montblanc-bootstrap', get_template_directory_uri().'/css/bootstrap.css');
    wp_enqueue_style( 'montblanc-flexslider', get_template_directory_uri().'/css/flexslider.css');
	wp_enqueue_style( 'montblanc-bxslider', get_template_directory_uri().'/css/jquery.bxslider.css');
	wp_enqueue_style( 'montblanc-main', get_template_directory_uri().'/css/'.$version.'-style.css');
	
	
	// Loads our main stylesheet.
	wp_enqueue_style( 'montblanc-style', get_stylesheet_uri(), array(), '2015-11-06' );
	
	
	

}
add_action( 'wp_enqueue_scripts', 'montblanc_scripts_styles' );


/*
Register Fonts
*/
function montblanc_fonts_url() {
    global $montblanc_options;
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'montblanc' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Droid Serif:400,400italic,700,700italic|Dancing Script:400,700|'.$montblanc_options['body-font2']['font-family'].':400,300,100,200,500,600,700,800,900&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}
/*
Enqueue scripts and styles.
*/
function montblanc_scripts() {
    global $montblanc_options;
    if($montblanc_options!=null && $montblanc_options['body-font2']['font-family'] != ''){
        wp_enqueue_style( 'montblanc-fonts', montblanc_fonts_url(), array(), '1.0.0' );
    }
}
add_action( 'wp_enqueue_scripts', 'montblanc_scripts' );


//Colors

add_action('wp_head','montblanc_color');

function montblanc_color() {
    global $montblanc_options;
    $main_color = $montblanc_options['main-color']; ?>

    <style> 

        .boxed ul.filter li a span, .boxed .statistic-section .statistic-box .statistic-post h2,.site-nav .social_icons a, .site-nav .navbar-nav > li.current-menu-item > a, .sub-menu a:hover,.contact-info i,.prev-post:hover i, .prev-post:hover span, .next-post:hover i, .next-post:hover span,.project-content h2,.project-title span,.project-content a,.blog-post a,blockquote p,.page-banner-section .page-banner-box p,.contact-us-section .contact-info h2,.pagination-list a:hover,.widget_recent_entries ul li a:hover, .widget_recent_comments ul li a:hover, .widget_archive ul li a:hover, .widget_categories ul li a:hover, .widget_meta ul li a:hover, .widget_pages ul li a:hover, .widget_rss ul li a:hover, .widget_nav_menu ul li a:hover, .product-categories li a:hover,.sidebar ul.recent-list li .post-content h2 a:hover,.sidebar .widget > h2,.blog-section .blog-post .blog-content .blog-title h2 a:hover,.blog-section .blog-post .blog-content .blog-title p a:hover,.team-section .team-post .team-content h2,.services-section .services-post i,.services-section .services-post h2,.portfolio-section.grid-style-fullwidth .portfolio-box.masonry .project-post .hover-box .inner-hover h2 a:hover,.half-section .text-sect .services-post h2,.half-section .text-sect .services-post i,.half-section .text-sect .contact-info h2,.navbar-right .dropdown-menu li a:hover, .statistic-post h2, .project-post .hover-box .inner-hover h2 a:hover, .half-section .text-sect .about-post i, .half-section .text-sect .about-post .post-content h2{
            color: <?php echo esc_attr($main_color); ?>;
        }
        .site-nav .navbar-nav > li > a:hover, .site-nav .navbar-nav > li.current-menu-item > a, .site-nav .navbar-nav > li.current-menu-parent >a {
            color: <?php echo esc_attr($main_color); ?> !important;
        }
        .sub-menu .current-menu-item >a{
            color: <?php echo esc_attr($main_color); ?> !important;
            text-decoration: underline;
        }
        .navbar-right .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {
          border-bottom: 1px solid  <?php echo esc_attr($main_color); ?>;
          color:  <?php echo esc_attr($main_color); ?>;
        }
        .navbar-right .dropdown-menu li a:hover{
            border-bottom: 1px solid  <?php echo esc_attr($main_color); ?>;
        }
        a.button-one, .bx-wrapper .bx-pager.bx-default-pager a:hover, .bx-wrapper .bx-pager.bx-default-pager a.active, input[type="submit"]{
            background: <?php echo esc_attr($main_color); ?>;
        }
        .half-section .image-sect .image-box .hover-box .inner-hover span.corner-border.left-top, .page-banner-section .page-banner-box span.corner-border.left-top{
              border-top: 1px solid <?php echo esc_attr($main_color); ?>;
              border-left: 1px solid <?php echo esc_attr($main_color); ?>;
        }
        .half-section .image-sect .image-box .hover-box .inner-hover span.corner-border.left-bottom , .page-banner-section .page-banner-box span.corner-border.left-bottom{
          border-bottom: 1px solid <?php echo esc_attr($main_color); ?>;
          border-left: 1px solid <?php echo esc_attr($main_color); ?>;
        }
        .half-section .image-sect .image-box .hover-box .inner-hover span.corner-border.right-top, .page-banner-section .page-banner-box span.corner-border.right-top {
          border-top: 1px solid <?php echo esc_attr($main_color); ?>;
          border-right: 1px solid <?php echo esc_attr($main_color); ?>;
        }
        .half-section .image-sect .image-box .hover-box .inner-hover span.corner-border.right-bottom , .page-banner-section .page-banner-box span.corner-border.right-bottom{
          border-bottom: 1px solid <?php echo esc_attr($main_color); ?>;
          border-right: 1px solid <?php echo esc_attr($main_color); ?>;
        }
        .sidebar form input[type="search"]:focus{
            border-color: <?php echo esc_attr($main_color); ?>;
        }
        <?php echo esc_attr($montblanc_options['custom-css']); ?>

    </style>

<?php 

}


function montblanc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Main Widget Area', 'montblanc' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Appears in main sidebar of the site.', 'montblanc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
    register_sidebar( array(
        'name'          => esc_html__( 'Shop Widget Area', 'montblanc' ),
        'id'            => 'sidebar-shop',
        'description'   => esc_html__( 'Appears in main sidebar of shop page.', 'montblanc' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(

        'name'          => esc_html__( 'Dropdown Cart', 'montblanc' ),
        'id'            => 'top-cart',
        'description'   => esc_html__( 'Appears in the Header cart.', 'montblanc' ),
        'before_widget' => '<aside id="%1$s" class=" shopping-cart-drop %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',

    ) );
}
add_action( 'widgets_init', 'montblanc_widgets_init' );

//pagination
function montblanc_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    $pagination = array(
		'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
		'format' 		=> '',
		'current' 		=> max( 1, get_query_var('paged') ),
		'total' 		=> $pages,
		'prev_text' => $prev,
        'next_text' => $next,
		'type'			=> 'plain',
        'after_page_number' => ' / ',
		'end_size'		=> 5,
		'mid_size'		=> 5
);
    $return =  paginate_links( $pagination );
	echo str_replace( "<ul class='page-numbers'>", '<ul class="pagination-list">', $return );
}
add_filter( 'loop_shop_columns', 'montblanc_loop_shop_columns', 1, 10 );
function montblanc_loop_shop_columns( $number_columns ) {
	return 3;
}
add_action( 'init', 'montblanc_remove_wc_breadcrumbs' );
function montblanc_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}
function is_realy_woocommerce_page () {
        if(  function_exists ( "is_woocommerce" ) && is_woocommerce()){
                return true;
        }
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
                        return true ;
                }
        }
        return false;
}

// Remove the product rating display on product loops
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

function montblanc_breadcrumb() {
       /* === OPTIONS === */
    $text['home']     = 'Home'; // text for the 'Home' link
    $text['category'] = 'Archive by Category "%s"'; // text for a category page
    $text['tax']       = 'Archive for "%s"'; // text for a taxonomy page
    $text['search']   = 'Search Results for "%s" Query'; // text for a search results page
    $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
    $text['author']   = 'Articles Posted by %s'; // text for an author page
    $text['404']      = 'Error 404'; // text for the 404 page
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter   = ''; // delimiter between crumbs
    $before      = '<li class="active">'; // tag before the current crumb
    $after       = '</li>'; // tag after the current crumb

    /* === END OF OPTIONS === */

    global $post;
    $homeLink = home_url('/') . '/';
    $linkBefore = '<li>';
    $linkAfter = '</li>';
    $linkAttr = ' rel="v:url" property="v:title"';
    $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
    if (is_home() || is_front_page()) {
        if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';
    } else {
        echo '<ul id="breadcrumbs" class="page-depth">' . sprintf($link, $homeLink, $text['home']) . $delimiter;
        if ( is_category() ) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
            }
            echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
        } elseif( is_tax() ){
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
            }

            echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;
        }elseif ( is_search() ) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;
        } elseif ( is_day() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;
        } elseif ( is_month() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;
        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;
        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
        } elseif ( is_attachment() ) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, $delimiter);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
            printf($link, get_permalink($parent), $parent->post_title);
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_page() && !$post->post_parent ) {
            if ($showCurrent == 1) echo $before . get_the_title() . $after;
        } elseif ( is_page() && $post->post_parent ) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) echo $delimiter;
            }
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_tag() ) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
        } elseif ( is_author() ) {
             global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;
        } elseif ( is_404() ) {
            echo $before . $text['404'] . $after;
        }
        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo esc_html__('Page','montblanc') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
        echo '</ul>';
    }
}



//Custom comment List:
function montblanc_theme_comment($comment, $args, $depth) {
	global $textdomain; 
     $GLOBALS['comment'] = $comment; ?>
     <!--=======  COMMENTS =========-->
    <li <?php comment_class('clearfix'); ?> id="comment-<?php comment_ID() ?>">
        <div class="comment-box">
            <?php if($comment->user_id!='0' and get_user_meta($comment->user_id, '_cmb_avatar' ,true)!=''){ ?>
                <img src="<?php echo bfi_thumb(get_user_meta($comment->user_id, '_cmb_avatar' ,true), array('width'=>80, 'height'=> 80)); ?>" />
            <?php }else{ ?>
            <?php echo get_avatar($comment,$size='80',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=80' ); ?>
            <?php } ?>
            <div class="comment-content">
                <h4><?php printf(esc_html__('%s','montblanc'), get_comment_author_link()) ?> <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></h4>
                <span><?php printf(esc_html__('%1$s at %2$s','montblanc'), get_comment_date(), get_comment_time()) ?></span>
                <?php comment_text() ?>
                <?php if ($comment->comment_approved == '0') : ?>
                     <em><?php esc_html_e('Your comment is awaiting moderation.','montblanc') ?></em>
                     <br />
                 <?php endif; ?>
            </div>
        </div>

	 
<?php

}
function montblanc_excerpt($limit = 30) {
 
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

function montblanc_thumbnail_url($size, $id){
    global $post;
    if($id==''){
        $id = $post->ID;
    }
    if($size==''){
        $url = wp_get_attachment_url( get_post_thumbnail_id($id) );
         return $url;
    }else{
        $url = wp_get_attachment_image_src( get_post_thumbnail_id($id), $size);
         return $url[0];
    }
   
}


// Remove Redux Ads
function montblanc_admin_styles() {
?>
<style type="text/css">
.rAds, .rAds span, .rAds div, .redux-notice {
display: none !important;
}
</style>
<?php
}
add_action('admin_head', 'montblanc_admin_styles');

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.5.2
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'montblanc_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function montblanc_register_required_plugins() {
    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
             // This is an example of how to include a plugin from a private repo in your theme.

		array(            
            'name'               => 'WPBakery Visual Composer', // The plugin name.
            'slug'               => 'js_composer', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/framework/plugins/js_composer.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
		array(            
            'name'               => 'Revolution Slider', // The plugin name.
            'slug'               => 'revslider', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/framework/plugins/revslider.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        array(            
            'name'               => 'QK Register Post Type', // The plugin name.
            'slug'               => 'qk-post_type', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory_uri() . '/framework/plugins/qk-post_type.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        array(            
            'name'               => 'QK Import', // The plugin name.
            'slug'               => 'qk-import', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory_uri() . '/framework/plugins/qk-import.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        // This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
        array(
            'name'      => 'Redux Framework',
            'slug'      => 'redux-framework',
            'required'  => true,
        ),
        array(
            'name'      => 'WooCommerce',
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
		array(
            'name'      => 'Widget Importer & Exporter',
            'slug'      => 'widget-importer-exporter',
            'required'  => false,
        ),
		
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'montblanc' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'montblanc' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'montblanc' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'montblanc' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'montblanc' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'montblanc' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'montblanc' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'montblanc' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'montblanc' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'montblanc' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'montblanc' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}
?>