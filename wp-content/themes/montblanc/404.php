<?php get_header(); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- page-banner-section
				================================================== -->
			<div class="section-content page-banner-section blog-banner">
				<div class="container">
					<div class="page-banner-box">
						<h1>404 ERROR</h1>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->

			<!-- blog-section
				================================================== -->
			<div class="section-content blog-section">
				<div class="container">
					<div class="row">

						<div class="col-md-9">

							<div class="blog-box">

								

								
					                <div class="not-found">
					                    <h1><?php esc_html_e('Nothing Found Here!','montblanc'); ?></h1>
					                    <h3><?php esc_html_e('Search with other string:', 'montblanc') ?></h3>
					                    <div class="search-form">
					                        <?php get_search_form(); ?>
					                    </div>
					                </div>
					           
							</div>

						</div>

						<div class="col-md-3">
							<?php get_sidebar(); ?>
						</div>

					</div>
				</div>
			</div>
			<!-- End blog-section -->

		</div>
		<!-- End content -->

<?php get_footer(); ?>