<div <?php post_class('blog-post'); ?>>
	<!-- Vimeo -->
	<?php if(get_post_meta(get_the_ID(), '_cmb_intro_video', true)!=''){ ?>
		
		<!-- youtube -->
		<?php echo wp_oembed_get(get_post_meta(get_the_ID(), '_cmb_intro_video', true)); ?>
		<!-- End youtube -->
	
	<?php } ?>
	<!-- End Vimeo -->
	
	<div class="blog-content">
		<div class="blog-title">
			<p> <?php the_time(get_option( 'date_format' )); ?> <?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?> <?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</div>
		<?php the_excerpt(); ?>
		<div class="social-box">
			<ul class="share-post">
			  <li class="facebook"><a onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook-square"></i></a></li>
              <li class="twitter"><a  onclick="window.open('http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo str_replace(" ","%20",get_the_title()); ?>"><i class="fa fa-twitter"></i></a></li>
              <li  class="pinterest"><a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a></li>
              <li  class="pinterest"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest"></i></a></li>
              <li class="linkedin"><a onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"><i class="fa fa-linkedin"></i></a></li>
			</ul>
		</div>
	</div>
</div>