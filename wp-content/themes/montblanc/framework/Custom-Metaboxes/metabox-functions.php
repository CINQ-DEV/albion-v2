<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {
   

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';
	
	$meta_boxes[] = array(
		'id'         => 'post_options',
		'title'      => 'Post Options',
		'pages'      => array('post'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
           
			
			
			array(
                'name' => 'oembed URL for post format video or audio',
                'desc' => 'Set Intro video, audio',
                'id'   => $prefix . 'intro_video',
                'type'    => 'oembed',
                
            ),
			array(
				'name'         => esc_html__( 'Post Slider for post gallery', 'montblanc' ),
				'desc'         => esc_html__( 'Set Intro slider gallery', 'montblanc' ),
				'id'           => $prefix . 'post_slider',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
			),
			array(
				'name'         => esc_html__( 'Heading Background', 'montblanc' ),
				'desc'         => esc_html__( 'Upload your single background iamge.', 'montblanc' ),
				'id'           => $prefix . 'breadcrumb_bg',
				'type'         => 'file',
				'preview_size' => array( 200, 100 ), // Default: array( 50, 50 )
			),
		),
	);
	
	$meta_boxes[] = array(
        'id'         => 'page_setting',
        'title'      => 'Page Setting',
        'pages'      => array('page'), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        //'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
        'fields' => array(
           
           
			array(
				'name'         => esc_html__( 'Heading Background', 'montblanc' ),
				'desc'         => esc_html__( 'Upload or add multiple images/attachments.', 'montblanc' ),
				'id'           => $prefix . 'breadcrumb_bg',
				'type'         => 'file',
				'preview_size' => array( 200, 100 ), // Default: array( 50, 50 )
			),
			array(
                'name' => 'Page Subtitle',
                'desc' => '',
                'id'   => $prefix . 'p_subtitle',
                'type'    => 'text',
                
            ),
        )
    );
	
	$meta_boxes[] = array(
		'id'         => 'portfolio_options',
		'title'      => 'Portfolio Options',
		'pages'      => array('portfolio'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
           
			array(
				'name'         => esc_html__( 'Portfolio Slider', 'montblanc' ),
				'desc'         => esc_html__( 'Upload or add multiple images/attachments.', 'montblanc' ),
				'id'           => $prefix . 'p_slider',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
			),
			
			array(
                'name' => 'Date',
                'desc' => '',
                'id'   => $prefix . 'p_date',
                'type'    => 'text',
                
            )
			,
			array(
                'name' => 'Portfolio Subtitle',
                'desc' => '',
                'id'   => $prefix . 'p_subtitle',
                'type'    => 'text',
                
            ),

            array(
                'name' => 'Alternative Grid Width',
                'desc' => 'Set Grid With on Alternative Template',
                'id'   => $prefix . 'portfolio_grid',
                'type'    => 'select',
                'options' => array(
                    array( 'name' => 'Grid size', 'value' => 'grid-sizer', ),
                    array( 'name' => 'Second width', 'value' => 'snd-size', ),
                    
                    )
            ),
		),
	);
	
	$meta_boxes[] = array(
		'id'         => 'lookbook_options',
		'title'      => 'Lookbook Options',
		'pages'      => array('lookbook'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
           
			
			
			array(
                'name' => 'Loobook description',
                'desc' => '',
                'id'   => $prefix . 'l_description',
                'type'    => 'wysiwyg',
                
            ),array(
                'name' => 'Loobook Url',
                'desc' => '',
                'id'   => $prefix . 'p_subtitle',
                'type'    => 'text',
                
            ),

           
		),
	);
	

	// Add other metaboxes as needed
	$meta_boxes['user_edit'] = array(
		'id'         => 'user_edit',
		'title'      => esc_html__( 'User Profile Metabox', 'montblanc' ),
		'pages'      => array( 'user' ), // Tells CMB to use user_meta vs post_meta
		'show_names' => true,
		'cmb_styles' => false, // Show cmb bundled styles.. not needed on user profile page
		'fields'     => array(
			array(
				'name'    => esc_html__( 'Avatar', 'montblanc' ),
				'desc'    => esc_html__( 'field description (optional)', 'montblanc' ),
				'id'      => $prefix . 'avatar',
				'type'    => 'file',
				'save_id' => true,
			),
			
			array(
				'name'     => esc_html__( 'Job', 'montblanc' ),
				'desc'     => esc_html__( 'Your Job (optional)', 'montblanc' ),
				'id'       => $prefix . 'user_Job',
				'type'     => 'text',
				'on_front' => false,
			),
		)
	);
	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}
