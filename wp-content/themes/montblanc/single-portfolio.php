<?php get_header(); ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}
	
?>
		<!-- content 
			================================================== -->
		<div id="content">

		<?php while(have_posts()) : the_post(); ?>
		<?php if($version=='vertical'){ ?>

		<!-- single-project-section2
				================================================== -->
			<div class="single-project single-project-section2">
				<div class="col-md-8">
				<?php 
					$gallery = get_post_meta(get_the_ID(), '_cmb_p_slider', true);
				?>
				<?php if(count($gallery)>0 and $gallery!=''){ ?>
				
				
				<div class="project-gallery ">
					
							<?php foreach($gallery as $img) {?>
							
								<img alt="<?php the_title(); ?>" src="<?php echo esc_attr($img); ?>" />
							
							<?php } ?>
						
				</div>
				
				<?php } ?>
				
					
					<div class="pre-next-post">
					  <div class="row">

					    <!--=======  PREVIS POST =========-->
					    <div class="col-sm-4">
					     	<?php
				              $prev_post = get_adjacent_post(false, '', true);
				              $next_post = get_adjacent_post(false, '', false); 
				            ?>
				            <?php if($prev_post!=null){ ?>
					        <div class="prev-post">
					        	<a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>">
					          <i class="fa fa-long-arrow-left"></i>  <span>previous project</span> 
					          </a>
					        </div>
					      	<?php } ?>
					    </div>
					    <div class="col-sm-4 text-center">
					    	 <a href="#.">back to works </a> 
					    </div>
					    <!--=======  NEXT POST =========-->
					    <div class="col-sm-4">
					      	<?php if($next_post!=null){ ?>
					        <div class="next-post ">
					        	<a title="<?php echo esc_attr($next_post->post_title); ?>" href="<?php echo esc_url(get_permalink($next_post->ID)); ?>">
					          <span>next project</span> <i class="fa fa-long-arrow-right"></i> 
					         </a>
					        </div>
					        <?php } ?>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="project-content">
						<div class="project-title">
							<h1><?php the_title(); ?></h1>
							<span>- 
								<?php 
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
									$item_skill = '';
										$i=1; foreach((array)$item_cats as $item_cat){
											if(count($item_cat)>0){
												
												if($i==1){
													$item_skill .= $item_cat->name;
												}else{
													$item_skill .= $item_cat->name . ' , ';
												}
												
											}
									$i++; }
									echo esc_html($item_skill);
								?>
							</span>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h2><?php esc_html_e('About Project','montblanc'); ?>: </h2>
								<?php the_content(); ?>
								
							</div>
							<div class="col-md-12">
								<h2><?php esc_html_e('Info','montblanc'); ?></h2>
								<p><?php echo esc_html(get_post_meta(get_the_ID(), '_cmb_p_date', true)); ?></p>
								<p>
									<?php 
									//var_dump($item_cat);
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
										$i=1; foreach((array)$item_cats as $item_cat){ ?>
										<a href="<?php echo esc_url(get_term_link($item_cat->slug, 'portfolio_tag'));?>" >#<?php echo esc_html($item_cat->name);?></a>
									<?php $i++; } ?>
								
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>


		<?php }else{ ?>
			<!-- single-project-section2
				================================================== -->
			<div class="section-content single-project single-project-section2">
				<?php 
					$gallery = get_post_meta(get_the_ID(), '_cmb_p_slider', true);
				?>
				<?php if(count($gallery)>0 and $gallery!=''){ ?>
				
				<?php if($version=='boxed'){ ?>
				<div class="project-gallery ">
					
							<?php foreach($gallery as $img) {?>
							
								<img alt="<?php the_title(); ?>" src="<?php echo esc_attr($img); ?>" />
							
							<?php } ?>
						
				</div>
				<?php }else{ ?>

				<div id="slider2" class="flexslider">
					<ul class="slides">
						<?php foreach($gallery as $img) {?>
						<li>
							<img alt="<?php the_title(); ?>" src="<?php echo esc_attr($img); ?>" />
						</li>
						<?php } ?>
						
					</ul>
				</div>
				<div class="container">
					<div id="carousel" class="flexslider">
						<ul class="slides">
							<?php foreach($gallery as $img) {?>
							<li>
								<img alt="<?php the_title(); ?>" src="<?php echo esc_attr(bfi_thumb( $img, array( 'width' => 390, 'height' => 300 ) )); ?>" />
							</li>
							<?php } ?>
							
						</ul>
					</div>
				</div>	
				<?php } ?>
				<?php } ?>
				<div class="container">
					<div class="project-content">
						<div class="project-title">
							<h1><?php the_title(); ?></h1>
							<span>- 
								<?php 
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
									$item_skill = '';
										$i=1; foreach((array)$item_cats as $item_cat){
											if(count($item_cat)>0){
												
												if($i==1){
													$item_skill .= $item_cat->name;
												}else{
													$item_skill .= $item_cat->name . ' , ';
												}
												
											}
									$i++; }
									echo esc_html($item_skill);
								?>
							</span>
						</div>
						<div class="row">
							<div class="col-md-8 col-sm-7">
								<h2><?php esc_html_e('About Project','montblanc'); ?>: </h2>
								<?php the_content(); ?>
								
							</div>
							<div class="col-md-4 col-sm-5">
								<h2><?php esc_html_e('Info','montblanc'); ?></h2>
								<p><?php echo esc_html(get_post_meta(get_the_ID(), '_cmb_p_date', true)); ?></p>
								<p>
									<?php 
									
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_tag');
										$i=1; foreach((array)$item_cats as $item_cat){ ?>
										<a href="<?php echo esc_url(get_term_link($item_cat->slug, 'portfolio_tag'));?>" >#<?php echo esc_html($item_cat->name);?></a>
									<?php $i++; } ?>
								
								</p>
							</div>
						</div>
					</div>
					<div class="pre-next-post">
					  <div class="row">

					    <!--=======  PREVIS POST =========-->
					    <div class="col-sm-4">
					     	<?php
				              $prev_post = get_adjacent_post(false, '', true);
				              $next_post = get_adjacent_post(false, '', false); 
				            ?>
				            <?php if($prev_post!=null){ ?>
					        <div class="prev-post">
					        	<a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>">
					          <i class="fa fa-long-arrow-left"></i>  <span>previous project</span> 
					          </a>
					        </div>
					      	<?php } ?>
					    </div>
					    <div class="col-sm-4 text-center">
					    	 <a href="#.">back to works </a> 
					    </div>
					    <!--=======  NEXT POST =========-->
					    <div class="col-sm-4">
					      	<?php if($next_post!=null){ ?>
					        <div class="next-post ">
					        	<a title="<?php echo esc_attr($next_post->post_title); ?>" href="<?php echo esc_url(get_permalink($next_post->ID)); ?>">
					          <span>next project</span> <i class="fa fa-long-arrow-right"></i> 
					         </a>
					        </div>
					        <?php } ?>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			<?php } ?>
		<?php endwhile; ?>
		</div>
		<!-- End content -->

<?php get_footer(); ?>