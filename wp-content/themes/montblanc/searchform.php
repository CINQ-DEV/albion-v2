<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Search Form Template
 *
 */
?>

   
	<div class="widget search-widget">
	  <form method="get" class="searchform" action="<?php echo esc_url( home_url('/') ); ?>" id="search">
	    <input type="search" placeholder="Search:" name="s">
	    <button type="submit">
	      <i class="fa fa-search"></i>
	    </button>
	  </form>
	</div>