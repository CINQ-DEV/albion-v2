<?php global $montblanc_options; ?>
		<!-- footer 
			================================================== -->
		<footer id="footer">
			<div class="container">
				<p class="copyright">
					<?php echo wp_kses_post($montblanc_options['footer-text']); ?>
				</p>
			</div>
		</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->
	
	<?php wp_footer(); ?>
</body>
</html>