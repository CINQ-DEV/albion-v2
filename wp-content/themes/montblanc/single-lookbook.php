<?php get_header(); ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}
	
?>
		<!-- content 
			================================================== -->
		<div id="content">

			<div class="section-content page-banner-section blog-banner single-post" <?php global $post; if(get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">

					<div class="page-banner-box">
						<p class="post-meta">  - Shop Collection</p>
						<h1><?php woocommerce_page_title(); ?></h1>
						<p>- <?php echo get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_p_subtitle', true); ?></p>
						
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>

		<?php while(have_posts()) : the_post(); ?>
		<?php if($version=='vertical'){ ?>

		<!-- single-project-section2
				================================================== -->
			<div class="single-project single-project-section2">
				<div class="col-md-8">
				<?php 
					$gallery = get_post_meta(get_the_ID(), '_cmb_p_slider', true);
				?>
				<?php if(count($gallery)>0 and $gallery!=''){ ?>
				
				
				<div class="project-gallery ">
					
							<?php foreach($gallery as $img) {?>
							
								<img alt="<?php the_title(); ?>" src="<?php echo esc_attr($img); ?>" />
							
							<?php } ?>
						
				</div>
				
				<?php } ?>
				
					
					<div class="pre-next-post">
					  <div class="row">

					    <!--=======  PREVIS POST =========-->
					    <div class="col-sm-4">
					     	<?php
				              $prev_post = get_adjacent_post(false, '', true);
				              $next_post = get_adjacent_post(false, '', false); 
				            ?>
				            <?php if($prev_post!=null){ ?>
					        <div class="prev-post">
					        	<a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>">
					          <i class="fa fa-long-arrow-left"></i>  <span>previous project</span> 
					          </a>
					        </div>
					      	<?php } ?>
					    </div>
					    <div class="col-sm-4 text-center">
					    	 <a href="#.">back to works </a> 
					    </div>
					    <!--=======  NEXT POST =========-->
					    <div class="col-sm-4">
					      	<?php if($next_post!=null){ ?>
					        <div class="next-post ">
					        	<a title="<?php echo esc_attr($next_post->post_title); ?>" href="<?php echo esc_url(get_permalink($next_post->ID)); ?>">
					          <span>next project</span> <i class="fa fa-long-arrow-right"></i> 
					         </a>
					        </div>
					        <?php } ?>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="project-content">
						<div class="project-title">
							<h1><?php the_title(); ?></h1>
							<span>- 
								<?php 
									$item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
									$item_skill = '';
										$i=1; foreach((array)$item_cats as $item_cat){
											if(count($item_cat)>0){
												
												if($i==1){
													$item_skill .= $item_cat->name;
												}else{
													$item_skill .= $item_cat->name . ' , ';
												}
												
											}
									$i++; }
									echo esc_html($item_skill);
								?>
							</span>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h2><?php esc_html_e('About Project','montblanc'); ?>: </h2>
								<?php the_content(); ?>
								
							</div>
							<div class="col-md-12">
								<h2><?php esc_html_e('Info','montblanc'); ?></h2>
								<p><?php echo esc_html(get_post_meta(get_the_ID(), '_cmb_p_date', true)); ?></p>
								<p>
									<?php 
									//var_dump($item_cat);
									$item_cats = get_the_terms(get_the_ID(), 'lookbook_tag');
										$i=1; foreach((array)$item_cats as $item_cat){ ?>
										<a href="<?php echo esc_url(get_term_link($item_cat->slug, 'lookbook_tag'));?>" >#<?php echo esc_html($item_cat->name);?></a>
									<?php $i++; } ?>
								
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>


		<?php }else{ ?>
			<!-- single-project-section2
				================================================== -->
			<div class="section-content single-lookbook single-project-section2">
				<div class="container">
					<div class="row">
				
				<div class="col-md-9">
				<div class="lookbook-gallery ">
					
							<?php the_content(); ?>
						
				</div>
				</div>
				
				<div class="col-md-3">
					<div class="project-content">
						<div class="project-title">
							<h1><?php the_title(); ?></h1>
							
							<?php echo wp_kses_post(get_post_meta(get_the_ID(), '_cmb_l_description', true)); ?>
							<div class="loobook-meta">
								<p>
									<span>CATEGORY: </span>
									<?php 
										$item_cats = get_the_terms(get_the_ID(), 'lookbook_category');
										$item_skill = '';
											$i=1; foreach((array)$item_cats as $item_cat){
												if(count($item_cat)>0){
													
													if($i==1){
														$item_skill .= $item_cat->name;
													}else{
														$item_skill .= $item_cat->name . ' , ';
													}
													
												}
										$i++; }
										echo esc_html($item_skill);
									?>
								</p>
								<p>
									<span>TAGS: </span>
									<?php 
									
									$item_cats = get_the_terms(get_the_ID(), 'lookbook_tag');
										$i=1; foreach((array)$item_cats as $item_cat){ ?>
										<a href="<?php echo esc_url(get_term_link($item_cat->slug, 'lookbook_tag'));?>" >#<?php echo esc_html($item_cat->name);?></a>
									<?php $i++; } ?>
								
								</p>
							</div>
							<a class="btn" href="#">View in shop</a>
						</div>
						
					</div>
				</div>
				</div>
					<div class="pre-next-post">
					  <div class="row">

					    <!--=======  PREVIS POST =========-->
					    <div class="col-sm-4">
					     	<?php
				              $prev_post = get_adjacent_post(false, '', true);
				              $next_post = get_adjacent_post(false, '', false); 
				            ?>
				            <?php if($prev_post!=null){ ?>
					        <div class="prev-post">
					        	<a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo esc_url(get_permalink($prev_post->ID)); ?>">
					          <i class="fa fa-long-arrow-left"></i>  <span>previous project</span> 
					          </a>
					        </div>
					      	<?php } ?>
					    </div>
					    <div class="col-sm-4 text-center">
					    	 <a href="#.">back to works </a> 
					    </div>
					    <!--=======  NEXT POST =========-->
					    <div class="col-sm-4">
					      	<?php if($next_post!=null){ ?>
					        <div class="next-post ">
					        	<a title="<?php echo esc_attr($next_post->post_title); ?>" href="<?php echo esc_url(get_permalink($next_post->ID)); ?>">
					          <span>next project</span> <i class="fa fa-long-arrow-right"></i> 
					         </a>
					        </div>
					        <?php } ?>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			<?php } ?>
		<?php endwhile; ?>
		</div>
		<!-- End content -->

<?php get_footer(); ?>