<?php 
/*
*Template Name: Page Builder
*/
?>
<?php get_header(); ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}

?>
		<!-- content 
			================================================== -->
		<div id="content">

			<!-- page-banner-section
				================================================== -->
			<div class="page-banner-section portfolio-banner" <?php global $post; if(get_post_meta($post->ID, '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta($post->ID, '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">
					<?php if($version=='vertical'){ ?>
						<div class="col-md-6">
							<h1 class="page-title"><?php single_post_title(); ?></h1>
						</div>
						<div class="col-md-6">
							<?php if(get_post_meta($post->ID, '_cmb_p_subtitle', true)!=''){ ?>
							<p class="subtitle">- <?php echo get_post_meta($post->ID, '_cmb_p_subtitle', true); ?></p>
							<?php } ?>
						</div>
						<?php }else{  ?>
					<div class="page-banner-box">
						<h1><?php single_post_title(); ?></h1>
						<?php if(get_post_meta($post->ID, '_cmb_p_subtitle', true)!=''){ ?>
						<p>- <?php echo get_post_meta($post->ID, '_cmb_p_subtitle', true); ?></p>
						<?php } ?>
						<?php 

						global $montblanc_options;
						if($montblanc_options['body_style']!=''){
							$version = $montblanc_options['body_style'];
						}else{
							$version = 'default';
						}
							
						?>
						<?php if($version!='boxed'){ ?>
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<!-- End page-banner-section -->

			<?php 
				while(have_posts()) : the_post();
					the_content();
				endwhile;
			?>
			

		</div>
		<!-- End content -->
<?php get_footer(); ?>