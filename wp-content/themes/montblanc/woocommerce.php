<?php get_header();  ?>
<?php 

global $montblanc_options;
if($montblanc_options['body_style']!=''){
	$version = $montblanc_options['body_style'];
}else{
	$version = 'default';
}

?>
		<!-- content 
			================================================== -->
		<div id="content">
		
		<?php if($version=='vertical'){ ?>
		

			<div class="blog-gal">
			<?php if(has_post_thumbnail()){ ?>
				<?php the_post_thumbnail(); ?>
			<?php }else{ ?>
				
			<?php } ?>
			</div>
		


			<!-- blog-section
			================================================== -->
			<div class="single-blog-section">


				<div class="blog-box">
					<div class="blog-title">
						<p> <?php the_time(get_option( 'date_format' )); ?> <?php esc_html_e('by','montblanc'); ?> <?php the_author_posts_link(); ?> <?php esc_html_e('in','montblanc');?> <?php the_category(', '); ?> <?php comments_popup_link(esc_html__('0 Comment ', 'montblanc'), esc_html__('1 Comment', 'montblanc'), esc_html__(' % Comments', 'montblanc')); ?></p>
						<h1><?php the_title(); ?></h1>
					</div>
					<div class="blog-post single-post">
						<?php the_content(); ?>
						<?php
                        $defaults = array(
                          'before'           => '<div id="page-links"><strong>Page: </strong>',
                          'after'            => '</div>',
                          'link_before'      => '<span>',
                          'link_after'       => '</span>',
                          'next_or_number'   => 'number',
                          'separator'        => ' ',
                          'nextpagelink'     => esc_html__( 'Next page','montblanc' ),
                          'previouspagelink' => esc_html__( 'Previous page','montblanc' ),
                          'pagelink'         => '%',
                          'echo'             => 1
                        );
                       ?>
                      <?php wp_link_pages($defaults); ?>
                      <?php if(has_tag()){ ?>
                        <?php the_tags('<ul class="single-post-tags"><li><span>Tags: </span> </li><li>',' </li>, <li>','</li></ul>'); ?>
                      <?php } ?>  
						
						

					</div>
					

					<?php comments_template(); ?>
				</div>


			</div>
			<!-- End blog-section -->

		<?php }else{ ?>
		<?php if($version=='boxed'){ ?>
			<!-- page-banner-section
				================================================== -->
			<div class="section-content gallery-post">
			
				<div class="container">
			
					<?php if(get_post_format()=='gallery'){ ?>
					<?php 
					$gallery = get_post_meta(get_the_ID(), '_cmb_post_slider', true);
					?>
					<?php if(count($gallery)>0 and $gallery!=''){ ?>

					<div class="post-slider">
						<?php foreach($gallery as $img) {?>
						<div class="slide"><img src="<?php echo esc_attr($img); ?>" alt="<?php the_title(); ?>" /></div>
						<?php } ?>
						
						
						
					</div>
					<?php } ?>
					<?php }else{ 
						the_post_thumbnail();
					} ?>
				
				</div>
				
			</div>
			<!-- End page-banner-section -->
			<?php }else{ ?>
				<!-- page-banner-section
				================================================== -->
			<div class="section-content page-banner-section blog-banner single-post" <?php global $post; if(get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_breadcrumb_bg', true)!=''){ ?> style="background-image: url(<?php echo get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_breadcrumb_bg', true); ?>); " <?php } ?>>
				<div class="container">

					<div class="page-banner-box">
						<p class="post-meta">  - Shop Collection</p>
						<h1><?php woocommerce_page_title(); ?></h1>
						<p>- <?php echo get_post_meta(get_option( 'woocommerce_shop_page_id' ), '_cmb_p_subtitle', true); ?></p>
						
						<span class="left-top corner-border"></span>
						<span class="left-bottom corner-border"></span>
						<span class="right-top corner-border"></span>
						<span class="right-bottom corner-border"></span>
					</div>
				</div>
			</div>
			<!-- End page-banner-section -->
			<?php } ?>
			<!-- blog-section
				================================================== -->
			<div class="section-content shop-section">
				<div class="container">
					<div class="row">
						<?php 

							if($version=='boxed' or $version=='vertical' or is_singular( 'product' )){ 
								$class="col-md-12";
							}else{
								$class="col-md-9";
							}

						?>
						<?php if ( !is_singular( 'product' ) ) { ?>
						<div class="col-md-3">
							<?php get_sidebar('shop'); ?>
						</div>

						<?php } ?>
						<div class="<?php echo esc_attr($class); ?>">
							
							<div class="blog-box">
								<?php if ( is_singular( 'product' ) ) { ?>
								<div class="blog-post single-post">
									
									<?php woocommerce_content();?>


								</div>
								<?php }else{ ?>

								<?php
									
									   //For ANY product archive.
									   //Product taxonomy, product search or /shop landing
										woocommerce_get_template( 'archive-product.php' );
									  
								?>

								<?php } ?>

							</div>
						
						</div>
						

					</div>
				</div>
			</div>
			<?php } ?>
			<!-- End blog-section -->
			
		</div>
		<!-- End content -->

<?php get_footer(); ?>