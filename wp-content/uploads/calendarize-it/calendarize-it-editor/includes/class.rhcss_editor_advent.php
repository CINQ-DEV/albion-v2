<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class rhcss_editor_advent extends module_righthere_css{
	function rhcss_editor_advent($args=array()){
		return $this->module_righthere_css($args);
	}
	
	function options($t=array()){
		$i = count($t);
		//---------------------------------									
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcadvent_default_fonts'; 
		$t[$i]->label 		= __('Default fonts','rhc');
		$t[$i]->options = array();								

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> 'rhcadvent_default_fonts_',
			'selector'	=> implode(',',array(
				'BODY .rhcalendar .fullCalendar .advent-day'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));

		$t[$i]->options[] =(object)array(
				'id'				=> 'rhcadvent_font_shadow',
				'type'				=> 'css',
				'label'				=> __('Text shadow','rhc'),
				'input_type'		=> 'textshadow',
				'opacity'			=> true,
				'selector'			=> 'BODY .rhcalendar .fullCalendar .advent-day',
				'property'			=> 'text-shadow',
				'real_time'			=> true,
				'btn_clear'			=> true
			);		

		/*
		$t[$i]->options[]=	(object)array(
				'id'				=> 'rhcadvent_door_top',
				'type'				=> 'css',
				'label'				=> __('Font top percentage','rhc'),
				'input_type'		=> 'number',
				'unit'				=> '%',
				'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'min'				=> '0',
				'max'				=> '100',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					'BODY .rhcalendar.advent_default .fullCalendar .advent-day',
					'BODY .rhcalendar.advent_slide .fullCalendar .advent-day'
				)),					
				'property'			=> 'top',
				'real_time'			=> true
			);					
		
		$t[$i]->options[]=	(object)array(
				'id'				=> 'rhcadvent_door_left',
				'type'				=> 'css',
				'label'				=> __('Font left percentage','rhc'),
				'input_type'		=> 'number',
				'unit'				=> '%',
				'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'min'				=> '0',
				'max'				=> '100',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					'BODY .rhcalendar.advent_default .fullCalendar .advent-day',
					'BODY .rhcalendar.advent_slide .fullCalendar .advent-day'
				)),					
				'property'			=> 'left',
				'real_time'			=> true
			);	
		*/					
		//---------------------------------									
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcadvent_bg'; 
		$t[$i]->label 		= __('Background','rhc');
		$t[$i]->options = array();	

		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> 'advent_bg',
			'selector'	=> implode(',',array(
				'BODY .rhcalendar.advent_slide .advent-holder .advent-overlay',
				'BODY .rhcalendar.advent_default .advent-holder.advent-closed .advent-overlay'
			))
			
				
		));		
		
		//---------------------------------									
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcadvent_door_bg'; 
		$t[$i]->label 		= __('Door animation','rhc');
		$t[$i]->options = array();	

		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Door image','rhc'),
			'prefix'	=> 'advent_door_bg',
			'selector'	=> "BODY .rhcalendar.advent_door .advent-holder .advent-overlay"			
		));		
		
		$t[$i]->options[]=	(object)array(
				'id'				=> 'rhcadvent_door_top',
				'type'				=> 'css',
				'label'				=> __('Font top percentage','rhc'),
				'input_type'		=> 'number',
				'unit'				=> '%',
				'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'min'				=> '0',
				'max'				=> '100',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					'BODY .rhcalendar.advent_door .fullCalendar .advent-day',
				)),					
				'property'			=> 'top',
				'real_time'			=> true
			);					
		
		$t[$i]->options[]=	(object)array(
				'id'				=> 'rhcadvent_door_left',
				'type'				=> 'css',
				'label'				=> __('Font left percentage','rhc'),
				'input_type'		=> 'number',
				'unit'				=> '%',
				'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'min'				=> '0',
				'max'				=> '100',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					'BODY .rhcalendar.advent_door .fullCalendar .advent-day',
				)),					
				'property'			=> 'left',
				'real_time'			=> true
			);			
				
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','rhc');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
}
?>