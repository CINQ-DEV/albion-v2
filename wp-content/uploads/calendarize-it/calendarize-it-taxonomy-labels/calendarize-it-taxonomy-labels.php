<?php

/**
Plugin Name: Customize Taxonomy Labels for Calendarize it! (BETA)
Plugin URI: http://plugins.righthere.com/calendarize-it/
Description: Provides a way to change the labels in Calendarize it! default Taxonomimes; Calendar, Venues and Organizers. 
Version: 1.0.0.59032
Author: Alberto Lau (RightHere LLC)
Author URI: http://plugins.righthere.com
 **/

if(defined('RHCTL_PATH')) throw new Exception( __('A duplicate of this addon/plugin is already active.','rhc') );
 
if(defined('RHC_ADDON_PATH')){
	define('RHCTL_PATH', trailingslashit(RHC_ADDON_PATH . dirname($addon)) ); 
	define("RHCTL_URL", trailingslashit(RHC_ADDON_URL . dirname($addon)) );
}else{
	define('RHCTL_PATH', plugin_dir_path(__FILE__) ); 
	define("RHCTL_URL", plugin_dir_url(__FILE__) );
} 


class plugin_rhc_custom_taxonomy_label {
	function plugin_rhc_custom_taxonomy_label(){
		add_action( 'plugins_loaded', array(&$this,'plugins_loaded'), 9 );
		//--- taxonomy labels
		add_filter( 'rhc_register_taxonomy', array( &$this, 'rhc_register_taxonomy'), 10, 5);
		//--- venue meta fields labels
		add_filter( 'rhc_venue_meta', array(&$this, 'rhc_venue_meta'), 9999, 1);//--- venue meta fields labels
		//--- organizer meta fields labels
		add_filter( 'rhc_organizer_meta', array(&$this, 'rhc_organizer_meta'), 9999, 1);
		//--- apply labels to quick icons (layout builder)
		add_filter( 'rhc_post_info_quick_icons', array( &$this, 'rhc_post_info_quick_icons'), 9999 );
		//--- apply to rhc-taxonomies
		add_filter( 'rhc-taxonomies', array( &$this, 'rhc_taxonomies') );
	}
	
	function rhc_taxonomies( $taxonomies ){
		$default_taxonomies = array(
			RHC_CALENDAR 	=> __('Calendar','rhc'),
			RHC_ORGANIZER	=> __('Organizer','rhc'),
			RHC_VENUE		=> __('Venues','rhc')
		);
		
		global $rhc_plugin;
		foreach( $default_taxonomies as $taxonomy => $default ){
			if( isset($taxonomies[$taxonomy])){
				$taxonomies[$taxonomy] = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', $taxonomy, 'singular_name' ), $taxonomies[$taxonomy], true);
			}
		}
		
		return $taxonomies;
	}
	
	function rhc_post_info_quick_icons( $q ){
		if( is_array($q) && count($q)>0){
			global $rhc_plugin;
			foreach($q as $i => $tab){
				//--- tabs
				if( 'venues'==$tab->id){
					$q[$i]->label = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', RHC_VENUE, 'singular_name' ), $q[$i]->label, true);
				}
				if( 'organizers'==$tab->id){
					$q[$i]->label = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', RHC_ORGANIZER, 'singular_name' ), $q[$i]->label, true);
				}
				if( 'calendars'==$tab->id){
					$q[$i]->label = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', RHC_CALENDAR, 'singular_name' ), $q[$i]->label, true);
				}
				//--- buttons
				if( is_array($tab->items) && count( $tab->items)>0){
					foreach( $tab->items as $i_items => $item ){
						//taxonomy
						if( property_exists($item,'post_extrainfo_taxonomy') && !empty($item->post_extrainfo_taxonomy)){
							$tab->items[$i_items]->label = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', $item->post_extrainfo_taxonomy, 'singular_name' ), $tab->items[$i_items]->label, true);
							$tab->items[$i_items]->post_extrainfo_label = $rhc_plugin->get_option( sprintf( 'rhctl_%s_%s', $item->post_extrainfo_taxonomy, 'singular_name' ), $tab->items[$i_items]->post_extrainfo_label, true);
						}
						//meta
						if( property_exists($item,'post_extrainfo_taxonomymeta') && !empty($item->post_extrainfo_taxonomymeta) ){
							$arr = explode('|', $item->post_extrainfo_taxonomymeta);
							if( count($arr) > 1 ){
								$meta = $arr[0];
								$taxonomy = $arr[1];		
								
								$option = sprintf( 'rhctl_meta_%s_%s', $taxonomy, $meta );
								
								$tab->items[$i_items]->label = $rhc_plugin->get_option( $option, $tab->items[$i_items]->label, true);
								$tab->items[$i_items]->post_extrainfo_label	= $rhc_plugin->get_option( $option, $tab->items[$i_items]->post_extrainfo_label, true);	
							}

						}
					}
				}
			}
		}
		return $q;
	}
	
	function rhc_organizer_meta( $fields ){
		return $this->rhc_taxonomy_meta( $fields, RHC_ORGANIZER );
	}
	
	function rhc_venue_meta( $fields ){
		return $this->rhc_taxonomy_meta( $fields, RHC_VENUE );
	}
	
	function rhc_taxonomy_meta( $fields, $taxonomy ){
		global $rhc_plugin;
		if( is_array($fields) && count($fields)>0 ){
			foreach( $fields as $i => $f ){
				if( !property_exists($f,'id') ) continue;
				if( !property_exists($f,'label') ) continue;
				$option = sprintf( 'rhctl_meta_%s_%s', $taxonomy, $f->id );
				$fields[$i]->label = $rhc_plugin->get_option( $option, $f->label, true );
				
				$description = $rhc_plugin->get_option( sprintf( 'rhctl_meta_desc_%s_%s', $taxonomy, $f->id ), '', true );
				if( !empty($description) || '.'==$description ){
					$description = '.'==$description ? '' : $description;
					$fields[$i]->description = $description;
				}
			}
		}
		return $fields;
	}
	
	function rhc_register_taxonomy( $args, $taxonomy, $object_type, $meta, $pluginpath ){
		global $rhc_plugin;

		$labels = array(
		    'name',
		    'singular_name',
		    'search_items',
		    'popular_items',
		    'all_items',
		    'edit_item', 
		    'update_item',
		    'add_new_item',
		    'new_item_name'
		);		

		foreach( $labels as $id ){
			$option = sprintf( 'rhctl_%s_%s', $taxonomy, $id );
			$default = isset($args['labels'][$id]) ? $args['labels'][$id] : null ;
			$args['labels'][$id] = $rhc_plugin->get_option( $option, $default, true );
		}

		return $args;
	}
	
	function plugins_loaded(){
		if(is_admin()){
			do_action('rh-php-commons');	
			
			require RHCTL_PATH.'includes/class.rhctl_options.php';
			new rhctl_options('rhc','manage_options',false);	
		}	
	}
	
}

new plugin_rhc_custom_taxonomy_label();
?>