<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class rhctl_options {
	function rhctl_options($plugin_id='rhc',$capability='manage_options',$open=false){
		if(current_user_can($capability)){
			$this->id = $plugin_id;
			$this->open = $open;
			add_filter("pop-options_{$this->id}",array(&$this,'options'),9999,1);		
		}
	}
	
	function options($t){
		$i = count($t);
		//---------------
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhctl'; 
		$t[$i]->open 			= $this->open; 
		$t[$i]->label 		= __('Taxonomy Labels','rhctl');
		$t[$i]->right_label	= __('Customize Calendarize It! taxonomy labels.','rhctl');
		$t[$i]->page_title	= __('Taxonomy Labels','rhctl');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();		
		$t[$i]->options[] =	(object)array(
					'type' 			=> 'subtitle',
					'label'			=> __('Taxonomy Labels','rhctl'),
					'description'	=> sprintf('<p>%s</p><p>%s</p><p>%s</p><p>%s</p>',
						__('Customize the label of the built-in Calendar, Venue and Organizer taxonomies without localization.','rhctl'),
						__('To change the slug you need to use the Permalink Settings tab.','rhctl'),
						__('For sites using multiple languages, you will need a translation plugin','rhctl'),
						__('Works best on new installations.  Please observe that existing events will preserve the old labels defined in the layout builder and require that they are changed manually.')
					)
				);	

		//------------------				
		global $rhc_plugin;
		
		if( '1'!= $rhc_plugin->get_option('disable_calendar','0',true)){
			$taxonomy = RHC_CALENDAR;		
					
			$t[$i]->options[] =	(object)array(
						'type' 			=> 'subtitle',
						'label'			=> __('Calendar Labels','rhctl')
					);	
				
			$labels = array(
					    'name' 			=> __( 'Calendars', 'rhc' ),
					    'singular_name' => __( 'Calendar', 'rhc' ),
					    'search_items' 	=>  __( 'Search Calendar','rhc' ),
					    'popular_items' => __( 'Popular Calendar','rhc' ),
					    'all_items' => __( 'All calendars', 'rhc' ),
					    'edit_item' => __( 'Edit calendar','rhc' ), 
					    'update_item' => __( 'Update calendar','rhc' ),
					    'add_new_item' => __( 'Add calendar','rhc' ),
					    'new_item_name' => __( 'New calendar','rhc' )
					);			
			
			foreach($labels as $id => $label){
				$t[$i]->options[] =	(object)array(
						'id'			=> sprintf( 'rhctl_%s_%s', $taxonomy, $id ),
						'type' 			=> 'text',
						'label'			=> $label,
						'el_properties' => array( 'placeholder' => esc_attr($label) ),
						'save_option'=>true,
						'load_option'=>true
					);
			}
		}
		
		if( '1'!= $rhc_plugin->get_option('disable_organizer','0',true)){
			$taxonomy = RHC_ORGANIZER;		
					
			$t[$i]->options[] =	(object)array(
						'type' 			=> 'subtitle',
						'label'			=> __('Organizer Labels','rhctl')
					);	
				
			$labels = array(
						'name' 				=> __( 'Organizers', 'rhc' ),
						'singular_name' 	=> __( 'Organizer', 'rhc' ),
						'search_items' 		=> __( 'Search Organizer' , 'rhc'),
						'popular_items' 	=> __( 'Popular Organizers' , 'rhc'),
						'all_items' 		=> __( 'All organizers' , 'rhc'),
						'edit_item' 		=> __( 'Edit Organizer' , 'rhc' ), 
						'update_item' 		=> __( 'Update Organizer' , 'rhc' ),
						'add_new_item' 		=> __( 'Add Organizer' , 'rhc' ),
						'new_item_name' 	=> __( 'New Organizer' , 'rhc' )
					);			
			
			foreach($labels as $id => $label){
				$t[$i]->options[] =	(object)array(
						'id'			=> sprintf( 'rhctl_%s_%s', $taxonomy, $id ),
						'type' 			=> 'text',
						'label'			=> $label,
						'el_properties' => array( 'placeholder' => esc_attr($label) ),
						'save_option'=>true,
						'load_option'=>true
					);
			}
			
			$t[$i]->options[] =	(object)array(
						'type' 			=> 'subtitle',
						'label'			=> __('Organizer Meta Field Labels','rhctl')
					);				
			
			$fields = array();
			include RHC_PATH.'includes/organizer_meta_fields.php';
			$fields = apply_filters('rhc_organizer_meta',$fields);
			if( is_array($fields) && count($fields)>0 ){
				foreach( $fields as $f ){
					if( !property_exists($f,'id') ) continue;
					if( !property_exists($f,'label') ) continue;
					//if( in_array($f->id,array('website_nofollow','image','glat','glon','ginfo','gzoom','websitelabel','content'))) continue;
					$id = $f->id;
					$label = $f->label;
					//--
					$t[$i]->options[] =	(object)array(
							'id'			=> sprintf( 'rhctl_meta_%s_%s', $taxonomy, $id ),
							'type' 			=> 'text',
							'label'			=> $label,
							'el_properties' => array( 'class'=>'widefat', 'placeholder' => esc_attr($label) ),
							'save_option'=>true,
							'load_option'=>true
						);
					//--
					$description = property_exists($f,'description') ? $f->description : '';
					$t[$i]->options[] =	(object)array(
							'id'			=> sprintf( 'rhctl_meta_desc_%s_%s', $taxonomy, $id ),
							'type' 			=> 'text',
							'label'			=> '',
							'el_properties' => array( 'class'=>'widefat', 'placeholder' => esc_attr($description) ),
							'save_option'=>true,
							'load_option'=>true
						);					
				}
			}			
		}
		
		if( '1'!= $rhc_plugin->get_option('disable_venue','0',true)){
			$taxonomy = RHC_VENUE;		
					
			$t[$i]->options[] =	(object)array(
						'type' 			=> 'subtitle',
						'label'			=> __('Venue Labels','rhctl')
					);	
				
			$labels = array(
						'name' 				=> __( 'Venues', 'rhc' ),
						'singular_name' 	=> __( 'Venue', 'rhc' ),
						'search_items' 		=> __( 'Search Venue', 'rhc' ),
						'popular_items' 	=> __( 'Popular Venue', 'rhc' ),
						'all_items' 		=> __( 'All venues', 'rhc' ),
						'edit_item' 		=> __( 'Edit venue', 'rhc' ), 
						'update_item' 		=> __( 'Update venue', 'rhc' ),
						'add_new_item' 		=> __( 'Add venue', 'rhc' ),
						'new_item_name' 	=> __( 'New venue', 'rhc' )
					);			
			
			foreach($labels as $id => $label){
				$t[$i]->options[] =	(object)array(
						'id'			=> sprintf( 'rhctl_%s_%s', $taxonomy, $id ),
						'type' 			=> 'text',
						'label'			=> $label,
						'el_properties' => array( 'placeholder' => esc_attr($label) ),
						'save_option'=>true,
						'load_option'=>true
					);
			}
			
			$t[$i]->options[] =	(object)array(
						'type' 			=> 'subtitle',
						'label'			=> __('Venue Meta Field Labels','rhctl')
					);				
			
			$fields = array();
			include RHC_PATH.'includes/venue_meta_fields.php';
			$fields = apply_filters('rhc_venue_meta',$fields);
			if( is_array($fields) && count($fields)>0 ){
				foreach( $fields as $f ){
					if( !property_exists($f,'id') ) continue;
					if( !property_exists($f,'label') ) continue;
					//if( in_array($f->id,array('website_nofollow','image','glat','glon','ginfo','gzoom','websitelabel','content'))) continue;
					$id = $f->id;
					$label = $f->label;
					//--
					$t[$i]->options[] =	(object)array(
							'id'			=> sprintf( 'rhctl_meta_%s_%s', $taxonomy, $id ),
							'type' 			=> 'text',
							'label'			=> $label,
							'el_properties' => array( 'class'=>'widefat', 'placeholder' => esc_attr($label) ),
							'save_option'=>true,
							'load_option'=>true
						);
					//--
					$description = property_exists($f,'description') ? $f->description : '';
					$t[$i]->options[] =	(object)array(
							'id'			=> sprintf( 'rhctl_meta_desc_%s_%s', $taxonomy, $id ),
							'type' 			=> 'text',
							'label'			=> '',
							'el_properties' => array( 'class'=>'widefat', 'placeholder' => esc_attr($description) ),
							'save_option'=>true,
							'load_option'=>true
						);					
				}
			}
		}		

				
		//------------------				
		
				
		$t[$i]->options[] =	(object)array(
					'type'=>'clear'
				);	
				
		$t[$i]->options[] =	(object)array(
					'type'	=> 'submit',
					'label'	=> __('Save','rhctl'),
					'class' => 'button-primary',
					'save_option'=>false,
					'load_option'=>false
				);					
				
		return $t;	
	}			
}
?>