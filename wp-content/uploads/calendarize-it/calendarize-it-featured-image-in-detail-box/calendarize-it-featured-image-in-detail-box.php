<?php

/**
Plugin Name: Featured image in detail box for Calendarize It!
Plugin URI: http://calendarize.it/
Description: If the event detail box image is not set, fallback to the featured image.  Use case: when using an existing post type that already have the featured image defined, as event.
Version: 1.0.0
Author: Alberto Lau (RightHere LLC)
Author URI: http://plugins.righthere.com
 **/
 
add_filter( 'get_post_metadata', 'rhc_featured_image_in_detail_box', 10, 4 );

function rhc_featured_image_in_detail_box( $r, $object_id, $meta_key, $single ){
	if( 'rhc_dbox_image'==$meta_key ){
		remove_filter( 'get_post_metadata', 'rhc_featured_image_in_detail_box', 10 );
		$value = get_post_meta( $object_id, 'rhc_dbox_image', true );
		add_filter( 'get_post_metadata', 'rhc_featured_image_in_detail_box', 10, 4 );
		
		if( empty( $value ) ){
			$object_thumbnail_id = get_post_thumbnail_id( $object_id );
			if( intval( $object_thumbnail_id ) ){
				return $object_thumbnail_id;
			}
		}
	}
	return $r;
}
?>